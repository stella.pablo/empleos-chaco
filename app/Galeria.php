<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $table = 'galerias';

    protected $fillable = ['nombre','emprendedor_id','oficio_id'];

    public $timestamps = false;

    public static function guardar($files,$id){

        foreach ($files as $file) {

            $nombre = $file->getClientOriginalName();
            $file->move('images/galeria', $file->getClientOriginalName());

            Galeria::create(['nombre'=>$nombre,'emprendedor_id'=>$id]);

        }

    }

}
