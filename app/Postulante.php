<?php

namespace App;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Postulante extends Model
{
    protected $table = 'postulantes';

    protected $fillable = ['nombre','apellido','sexo','dni','estado_civil','foto','telefono_fijo',
                            'fecha_nac', 'telefono_movil','direccion','localidad_id','cv','user_id'];



    public function usuario()
    {
        return $this->belongsTo('App\User');
    }

    public static function perfil(Request $request){

        $post = Postulante::where('user_id','=',auth()->user()->id)->first();

        if($post != NULL){

            $row = Postulante::where('user_id', '=', Auth::user()->id)->update([
                                            'nombre'=> $request->input('nombre'),
                                            'apellido'=> $request->input('apellido'),
                                            'sexo' => $request->input('sexo'),
                                            'dni' => $request->input('dni'),
                                            'estado_civil' => $request->input('estado_civil'),
                                            'fecha_nac'=> $request->input('fecha_nac'),
                                            'telefono_movil' => $request->input('telefono_movil'),
                                            'telefono_fijo' => $request->input('telefono_fijo'),
                                            'direccion' => $request->input('direccion'),
                                            'localidad_id' => $request->input('localidad_id')]);


        }else{

            $row = Postulante::create(['user_id'=>Auth::user()->id,
                'nombre'=> $request->input('nombre'),
                'apellido'=> $request->input('apellido'),
                'sexo' => $request->input('sexo'),
                'dni' => $request->input('dni'),
                'estado_civil' => $request->input('estado_civil'),
                'telefono_fijo' => $request->input('telefono_fijo'),
                'fecha_nac'=> $request->input('fecha_nac'),
                'telefono_movil' => $request->input('telefono_movil'),
                'direccion' => $request->input('direccion'),
                'localidad_id' => $request->input('localidad_id')
            ]);


        }

    }

}
