<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Empresa extends Model
{
    protected  $table = 'empresas';

    protected $fillable = ['razon_social','direccion','cuit','email','area_id','telefono',
                            'localidad_id','provincia_id','sinopsis','logo'];


    public function ofertas()
    {
        return $this->hasMany('App\Oferta');
    }

    public static function cargar($request){

        $nombre = NULL;

        if($request->logo){

            $file = $request->file('logo');
            $nombre = $file->getClientOriginalName();
            Image::make($request->file('logo'))->resize(165, 165)->save('images/empresas/'. $nombre);

        }

        Empresa::create(['razon_social'=>$request->razon_social,
                            'direccion'=>$request->direccion,
                            'cuit'=>$request->cuit,
                            'email'=>$request->email,
                            'area_id'=>$request->area_id,
                            'telefono'=>$request->telefono,
                            'localidad_id'=>$request->localidad_id,
                            'provincia_id'=>$request->provincia_id,
                            'sinopsis'=>$request->sinopsis,
                            'logo' => $nombre
        ]);
    }

}
