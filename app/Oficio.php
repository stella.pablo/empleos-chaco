<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoriaOficio;
use Intervention\Image\Facades\Image;

class Oficio extends Model
{
    protected $table = 'oficios';

    protected $fillable = ['nombre', 'responsable', 'telefono', 'email',
        'direccion', 'categoria_oficio_id', 'logo', 'slug','rating','cantidad'];


    public static function cargar($request)
    {


        $nombre = Oficio::savePerfil($request);

        $row = Oficio::create(['responsable' => $request->responsable,
            'email' => $request->email,
            'categoria_oficio_id' => $request->categoria_oficio_id,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'logo' => $nombre,
            'slug' => str_slug(CategoriaOficio::find($request->categoria_oficio_id)->nombre . '-' . $request->nombre)
        ]);


        return $row->id;

    }

    public static function actualizar($id, $request)
    {

        $oficio = Oficio::find($id);
        $nombre = $oficio->logo;

        $nombre = Oficio::savePerfil($request);


        $row = Oficio::where('id', '=', $id)->update(['responsable' => $request->responsable,
            'email' => $request->email,
            'categoria_oficio_id' => $request->categoria_oficio_id,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'logo' => $nombre,
            'slug' => str_slug(CategoriaOficio::find($request->categoria_oficio_id)->nombre . '-' . $request->nombre)
        ]);
    }

    public static function savePerfil($request){

        $nombre = NULL;


        if ($request->hasFile('logo')) {

            $file = $request->file('logo');
            $nombre = rand(10000,99999).'.'.$request->file('logo')->getClientOriginalExtension();
            Image::make($request->file('logo'))->fit(170)->save('images/oficios/' . $nombre);

        }

        return $nombre;
    }
}
