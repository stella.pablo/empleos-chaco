<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Emprendedor extends Model
{
    protected $table = 'emprendedores';

    protected $fillable = ['nombre','responsable','cuit','telefono','email',
                            'direccion','categoria_emprendedor_id', 'logo','descripcion','slug'];


    public static function cargar($request)
    {

        $nombre = NULL;

        if ($request->logo) {

            $file = $request->file('logo');
            $nombre = rand(10000,99999).'.'.$request->file('logo')->getClientOriginalExtension();
            Image::make($request->file('logo'))->fit(170)->save('images/emprendedores/' . $nombre);

        }

       $row =  Emprendedor::create(['nombre' => $request->nombre,
            'responsable' => $request->responsable,
            'cuit' => $request->cuit,
            'email' => $request->email,
            'categoria_emprendedor_id' => $request->categoria_emprendedor_id,
            'telefono' => $request->telefono,
            'direccion' => $request->direccion,
            'descripcion' => $request->descripcion,
            'logo' => $nombre,
            'slug' => str_slug($request->nombre)
        ]);


        return $row->id;

    }

    public static function actualizar($id,$request)
    {

        $emp = Emprendedor::find($id);
        $nombre = $emp->logo;


        if ($request->logo) {

            $file = $request->file('logo');
            $nombre = rand(10000,99999).'.'.$request->file('logo')->getClientOriginalExtension();
            Image::make($request->file('logo'))->fit(300)->save('images/emprendedores/' . $nombre);

        }

        $row =  Emprendedor::where('id','=',$id)->update(['nombre' => $request->nombre,
                                                        'responsable' => $request->responsable,
                                                        'cuit' => $request->cuit,
                                                        'email' => $request->email,
                                                        'categoria_emprendedor_id' => $request->categoria_emprendedor_id,
                                                        'telefono' => $request->telefono,
                                                        'direccion' => $request->direccion,
                                                        'descripcion' => $request->descripcion,
                                                        'logo' => $nombre,
                                                        'slug' => str_slug($request->nombre)
        ]);



    }
}
