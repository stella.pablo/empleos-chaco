<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Experiencia extends Model
{
    protected $table = 'experiencias';

    protected $fillable = ['empresa','localidad_id','direccion','puesto','area_id','fecha_inicio','fecha_finalizacion','postulante_id','descripcion','provincia_id'];


    public static function agregar($request){

        $row = Experiencia::create([
            'empresa'=> $request->input('empresa'),
            'localidad_id'=> $request->input('localidad_id'),
            'provincia_id'=> $request->input('provincia_id'),
            'direccion'=> $request->input('direccion'),
            'puesto'=> $request->input('puesto'),
            'area_id'=> $request->input('area_id'),
            'fecha_inicio'=> $request->input('fecha_inicio'),
            'fecha_finalizacion'=> $request->input('fecha_finalizacion'),
            'descripcion'=> $request->input('descripcion'),
            'postulante_id'=> Auth::user()->id]);


        return $row->id;
    }
}
