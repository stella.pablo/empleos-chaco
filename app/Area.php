<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Area extends Model
{
    protected $table = 'areas';

    protected $fillable = ['nombre','icono'];

    public static function getCategoriasConTrabajo(){

        $categorias = DB::table('ofertas')
            ->join('areas','ofertas.area_id','=','areas.id')
            ->select('areas.nombre','areas.icono', DB::raw('count(*) as total'))
            ->groupBy('area_id')->get();

        return $categorias;

    }
}
