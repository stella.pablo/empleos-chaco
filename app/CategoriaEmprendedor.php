<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaEmprendedor extends Model
{
    protected $table = 'categorias_emprendedores';

    protected $fillable = ['descripcion'];
}
