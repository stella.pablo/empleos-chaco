<?php

function flash($message, $level = 'info')
{
    session()->flash('flash_message',$message);
    session()->flash('flash_message_level',$level);

}

function getColor($modalidad){

    if($modalidad == 'Full Time'){
        return '2';
    }

    if($modalidad == 'Part Time'){
        return '1';
    }

    if($modalidad == 'Contrato'){
        return '3';
    }

    if($modalidad == 'Freelance'){
        return '4';
    }
}

function getUbicacion($prov,$loc){


    if($prov == NULL){

        return \App\Localidad::find($loc)->nombre;

    }else{

        return \App\Localidad::find($loc)->nombre . ", " . \App\Provincia::find($prov)->nombre;


    }


}



