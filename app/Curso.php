<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Curso extends Model
{
    protected $table = 'cursos';

    protected $fillable = ['institucion','nombre','descripcion','postulante_id','localidad_id','provincia_id'];

    public static function agregar($request){

        Curso::create([
                    'institucion'=> $request->input('institucion'),
                    'nombre'=> $request->input('nombre'),
                    'descripcion'=> $request->input('descripcion'),
                    'localidad_id'=> $request->input('localidad_id'),
                    'provincia_id'=> $request->input('provincia_id'),

            'postulante_id'=> Auth::user()->id,
            ]);

    }
}
