<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Postulados extends Model
{
    protected $table = 'postulados';

    protected $fillable = ['oferta_id','postulante_id'];


    public static function postular($oferta){

        $check = Postulados::where('postulante_id','=',auth()->user()->id)
            ->where('oferta_id','=',$oferta)->count();

        if($check == 0){

            Postulados::create(['oferta_id'=>$oferta,'postulante_id'=>auth()->user()->id]);

            return 0;

        }else{

            return 1;

        }

    }

    public function listado(){

        return DB::table('postulados')->join('ofertas','postulados.oferta_id','=','ofertas.id')
                                              ->join('postulantes','postulados.postulante_id','=','postulantes.id')
                                              ->select('ofertas.id as oferta_id','ofertas.descripcion_corta','ofertas.modalidad',
                                                  'postulantes.nombre','postulantes.apellido','ofertas.empresa_id','postulados.created_at',
                                                  'postulantes.id as usuario_id','postulantes.cv')
                                              ->get();

    }
}
