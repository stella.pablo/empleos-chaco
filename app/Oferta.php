<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $table = 'ofertas';

    protected $dates = ['fecha_inicio'];


    protected $fillable = ['empresa_id','puestos','descripcion_corta','descripcion_larga','modalidad','fecha_inicio','fecha_finalizacion','area_id',
                            'localidad_id','provincia_id','slug','destacar','urgente'];

    public static function cargar($request){


        Oferta::create(['empresa_id'=>$request->empresa_id,'puestos'=>$request->puestos,
            'descripcion_corta'=>$request->descripcion_corta,'descripcion_larga'=>$request->descripcion_larga,
            'modalidad'=>$request->modalidad,
            'fecha_inicio'=>$request->fecha_inicio,
            'area_id'=>$request->area_id,
            'localidad_id'=>$request->localidad_id,'provincia_id'=>$request->provincia_id,
            'slug'=> str_slug($request->descripcion_corta),
            'destacar'=>$request->destacar,
            'urgente'=>$request->urgente]
        );

    }


    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function recientes(){

        return Oferta::where('destacar',NULL)
                        ->where('deleted_at', NULL)
                        ->orderBy('urgente','1')
                        ->orderBy('id','DESC')
                        ->take(10)
                        ->get();
    }

    public function scopeOrdered($query)
    {
        return $query->where('destacar',NULL)
                     ->where('deleted_at', NULL)
                     ->orderBy('id','DESC')
                     ->take(10);
    }

    public function scopeUrgente($query)
    {
        return $query->orderBy('urgente','1');
    }


    public function destacado(){

        return Oferta::where('destacar','=',1)
                        ->where('deleted_at', NULL)
                        ->orderBy('id','DESC')
                        ->take(5)
                        ->get();

    }
}
