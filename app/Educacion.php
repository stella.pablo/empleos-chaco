<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class Educacion extends Model
{
    protected $table = 'educacions';

    protected $fillable = ['nivel_educativo','titulo','estado','institucion','provincia_id','localidad_id','fecha_inicio',
                            'fecha_finalizacion','postulante_id'];


    public static function agregar($request){

        $row = Educacion::create([
                            'nivel_educativo'=> $request->input('nivel_educativo'),
                            'titulo'=> $request->input('titulo'),
                            'estado'=> $request->input('estado'),
                            'institucion'=> $request->input('institucion'),
                            'provincia_id'=> $request->input('provincia_id'),
                            'localidad_id'=> $request->input('localidad_id'),
                            'fecha_inicio'=> $request->input('fecha_inicio'),
                            'fecha_finalizacion'=> $request->input('fecha_finalizacion'),
                            'postulante_id'=> Auth::user()->id]);


        return $row->id;
    }
}
