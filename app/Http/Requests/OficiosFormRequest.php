<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OficiosFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'responsable' => 'required|string',
            'direccion' => 'required|string',
            'telefono' => 'required',
            'categoria_oficio_id' =>'required',
            'logo' => 'mimes:jpg,jpeg,png,JPG',
            'logo' => 'between:0.1kb,2048kb'
        ];
    }
}
