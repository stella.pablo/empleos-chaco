<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'consulta' => 'required',
            'motivo' => 'required',
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'=>'Nombre es campo obligatorio',
            'email.required'=>'Email es campo obligatorio',
            'email.email'=>'Ingrese un email valido',
            'motivo.required'=>'El motivo es un campo obligatorio',
            'consulta.required'=>'Consulta es un campo obligatorio'
        ];
    }
}
