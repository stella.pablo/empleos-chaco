<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostulanteFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'dni' =>'required|numeric',
            'estado_civil' =>'required',
            'fecha_nac' =>'required',
            'localidad_id'=>'required',
            'direccion' => 'required',
            'sexo' =>'required',
            'foto'=>'image',
            'cv' => 'mimes:doc,docx,pdf,odt',
            'cv' => 'between:0.1kb,2048kb'
        ];
    }

    public function messages()
    {
        return [
          'nombre.required'=>'Nombre obligatorio',
          'apellido.required'=>'Apellido obligatorio',
          'dni.required'=>'DNI obligatorio',
          'dni.numeric'=>'DNI es numérico',
          'fecha_nac.required'=>'Fecha de nacimiento obligatorio',
          'cv.mimes'=>'El CV solo pueden ser en formato PDF, ODT y DOC',
          'cv.between' => 'El documento no debe ser mayor 2MB'
        ];
    }
}
