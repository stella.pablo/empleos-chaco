<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmprendedoresFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=> 'required',
            'responsable'=> 'required',
            'telefono'=> 'required',
            'email'=> 'required',
            'galeria.*' => 'mimes:jpg,jpeg,png',
            'galeria.*' => 'between:0.1kb,2048kb'

        ];
    }

    public function messages()
    {
       return[
           'galeria.*' => 'Deben ser solo en formato png,jpg',
       ];
    }
}
