<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EducacionFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nivel_educativo' =>'required',
            'titulo' => 'required',
            'estado'=> 'required',
            'institucion' => 'required',
            'provincia_id' => 'required',
            'localidad_id' => 'required',
        ];

    }
}
