<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OfertaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa_id' =>'required',
            'descripcion_corta'=>'required',
            'puestos'=>'required',
            'fecha_inicio' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'empresa_id.required' =>'Seleccione Empresa',
            'descripcion_corta.required'=>'Titulo campo obligatorio',
            'puestos.required'=>'Puestos campo obligatorio',
            'fecha_inicio.required' => 'Fecha de inicio obligatorio'
        ];
    }
}
