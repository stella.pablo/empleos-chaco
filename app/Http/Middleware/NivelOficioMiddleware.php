<?php

namespace App\Http\Middleware;

use Closure;

class NivelOficioMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->nivel == 1  | auth()->user()->nivel == 3){
            return $next($request);
        }

        abort(403);
    }
}
