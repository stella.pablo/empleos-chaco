<?php

namespace App\Http\Middleware;

use Closure;

class NivelEmprenderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->nivel == 2 | auth()->user()->nivel == 3){
            return $next($request);
        }else{

            abort(403);
        }


    }
}
