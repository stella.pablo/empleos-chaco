<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



    Route::get('/', 'HomeController@index');

    Route::get('informatorio/guiartec',function(){
        return redirect('http://159.203.5.192/GuiArtec/default/index-html');
    });

    Route::get('informatorio/informarq',function(){
        return redirect('http://159.203.5.192/InformarQ/default/index');
    });
    Route::get('informatorio/chuwal',function(){
        return redirect('http://159.203.5.192/Chuwal/chuInterface/chuHome');
    });
    Route::get('informatorio/brainchaco',function(){
        return redirect('http://159.203.5.192/BrainChaco/default/index');
    });
    Route::get('informatorio/bienall',function(){
        return redirect('http://159.203.5.192/BienAll/default/index');
    });
    Route::get('informatorio/esqltor',function(){
        return redirect('http://159.203.5.192/esQltoR/default/index');
    });


    Route::get('contacto', 'HomeController@contacto');
    Route::get('empresas', 'HomeController@contactoEmpresa');
    Route::post('consulta', ['as' => 'consulta.store', 'uses' => 'HomeController@consulta']);
    Route::get('registro', ['as' => 'registro', 'uses' => 'HomeController@registrarMapa']);
    Route::post('registro', ['as' => 'store.geolocalizacion', 'uses' => 'HomeController@saveGeolocalizacion']);

    Route::group(['prefix' => 'joveneslideres'], function() {

        Route::get('/', 'HomeController@lideres');
        Route::get('financiamiento', 'HomeController@financiamiento');
        Route::get('agenda', 'HomeController@agenda');
        Route::get('inscripcion', 'HomeController@inscripcion');
        Route::get('contacto', 'HomeController@contacto2');



    });


    Route::get('guia/emprendedores', ['as' => 'guia.emprendedores', 'uses' => 'HomeController@emprendedores']);
    Route::get('emprendimiento/{slug}', ['as' => 'guia.emprendimiento', 'uses' => 'HomeController@emprendimiento']);

    Route::get('guia/oficios', ['as' => 'guia.oficios', 'uses' => 'HomeController@oficios']);
    Route::get('blog', ['as' => 'guia.blog', 'uses' => 'BlogController@get']);
    Route::get('blog/post/{slug}', ['as' => 'guia.blog.post', 'uses' => 'BlogController@getPost']);
    Route::get('mapa', 'HomeController@mapaGeolocalizacion');



    Route::auth();

    Route::get('empresa/{id}', ['as' => 'empresa.show', 'uses' => 'HomeController@getInfoEmpresa']);

    Route::get('puesto/{slug}', ['as' => 'puesto.show', 'uses' => 'HomeController@getPuesto']);

    Route::get('postularse/{id}', ['as' => 'postularse', 'uses' => 'PostulantesController@postularse']);


    Route::get('postulantes/cursos', 'PostulantesController@cursos');
    Route::post('postulantes/cursos', ['as' => 'postulantes.store.curso', 'uses' => 'PostulantesController@storeCurso']);

    Route::get('postulantes/cursos/{id}/edit', ['as' => 'postulantes.edit.cursos', 'uses' => 'PostulantesController@editCurso']);
    Route::put('postulantes/cursos/{id}/update/', ['as' => 'postulantes.update.cursos', 'uses' => 'PostulantesController@updateCurso']);


    Route::get('postulantes/experiencias_laborales', 'PostulantesController@experiencia');
    Route::post('postulantes/experiencias_laborales', ['as' => 'postulantes.store.experiencia', 'uses' => 'PostulantesController@storeExperiencia']);

    Route::get('postulantes/experiencias_laborales/{id}/edit', ['as' => 'postulantes.edit.experiencias_laborales', 'uses' => 'PostulantesController@editExperiencia']);
    Route::put('postulantes/experiencias_laborales/{id}/update/', ['as' => 'postulantes.update.experiencias_laborales', 'uses' => 'PostulantesController@updateExperiencia']);


    Route::get('postulantes/educacion', 'PostulantesController@educacion');
    Route::post('postulantes/educacion', ['as' => 'postulantes.store.educacion', 'uses' => 'PostulantesController@storeEducacion']);

    Route::get('postulantes/educacion/{id}/edit', ['as' => 'postulantes.edit.educacion', 'uses' => 'PostulantesController@editEducacion']);
    Route::put('postulantes/educacion/{id}/update/', ['as' => 'postulantes.update.educacion', 'uses' => 'PostulantesController@updateEducacion']);

    Route::get('postulantes', 'PostulantesController@badroute');

    Route::resource('postulantes', 'PostulantesController',['only' => ['create','store','update']]);

    //Route::get('resultados', 'PostulantesController@busquedas');
    Route::post('busquedas/resultados', ['as' => 'busquedas', 'uses' => 'HomeController@busquedas']);
    Route::get('busquedas/resultados', ['as' => 'busquedas', 'uses' => 'HomeController@busquedasGet']);

    Route::get('busquedas/emprendimientos', ['as' => 'busquedas.emprendimientos', 'uses' => 'HomeController@emprendedores']);
    Route::post('busquedas/emprendimientos', ['as' => 'busquedas.emprendimientos', 'uses' => 'HomeController@buscarEmprendimientos']);

    Route::get('busquedas/oficios', ['as' => 'busquedas.oficios', 'uses' => 'HomeController@oficios']);
    Route::post('busquedas/oficios', ['as' => 'busquedas.oficios', 'uses' => 'HomeController@buscarOficios']);
    Route::get('oficios/{id}', ['as' => 'oficio.ver', 'uses' => 'HomeController@getOficio']);


    Route::get('gestion/login', 'UsuariosController@getLogin');
    Route::get('admin', 'UsuariosController@getLogin');
    Route::post('gestion/authenticate', 'UsuariosController@authenticate');

    Route::post('rating', ['as' => 'rating', 'uses' => 'HomeController@rating']);



    Route::group(['prefix' => 'admin','middleware' => 'administrador'], function() {

        Route::get('galeria/delete/{id}/{emp}', ['as' => 'admin.galeria.delete', 'uses' => 'EmprendedoresController@eliminarImagen']);

        Route::post('upload', ['as' => 'upload-post', 'uses' =>'ImageController@postUpload']);
        Route::post('upload/delete', ['as' => 'upload-remove', 'uses' =>'ImageController@deleteUpload']);


        Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'HomeController@dashboard']);

        //mapa
        Route::get('geolocalizacion', ['as' => 'admin.puntos', 'uses' => 'MapasController@index']);
        Route::get('geolocalizacion/{id}/activar', ['as' => 'admin.puntos.activar', 'uses' => 'MapasController@activar']);
        Route::get('geolocalizacion/{id}/eliminar', ['as' => 'admin.puntos.destroy', 'uses' => 'MapasController@destroy']);

        Route::get('categorias-emprendedores/{id}/destroy', ['as' => 'admin.categorias-emprendedores.delete', 'uses' => 'CatEmprendedoresController@destroy']);

        Route::resource('emprendedores', 'EmprendedoresController');
        Route::resource('categorias-emprendedores', 'CatEmprendedoresController');

        Route::get('consultas/{id}/show', ['as' => 'admin.consultas.show', 'uses' => 'UsuariosController@getConsultas']);
        Route::get('oficios/{id}/destroy', ['as' => 'admin.oficios.delete', 'uses' => 'OficiosController@destroy']);
        Route::get('categorias-oficios/{id}/destroy', ['as' => 'admin.categorias-oficios.delete', 'uses' => 'CatOficiosController@destroy']);


        Route::resource('oficios', 'OficiosController');
        Route::resource('categorias-oficios', 'CatOficiosController');


        Route::resource('empresas', 'EmpresasController');
        Route::resource('areas', 'AreasController');
        Route::resource('ofertas', 'OfertasController');
        Route::resource('categorias-oficios', 'CatOficiosController');
        Route::resource('blog', 'BlogController',['only' => ['index','create','store','update','edit','destroy']]);
        Route::get('blog/{id}/destroy', ['as' => 'admin.blog.destroy', 'uses' => 'BlogController@destroy']);


        Route::get('consultas', ['as' => 'admin.consultas', 'uses' => 'HomeController@getConsultas']);
        Route::get('ofertas/{id}/destroy', ['as' => 'admin.ofertas.destroy', 'uses' => 'OfertasController@destroy']);
        Route::get('ofertas/destacar/{id}', ['as' => 'admin.ofertas.destacar', 'uses' => 'OfertasController@destacar']);
        Route::get('ofertas/nodestacar/{id}', ['as' => 'admin.ofertas.nodestacar', 'uses' => 'OfertasController@noDestacar']);

        //gestion de usuarios
        Route::get('usuarios', ['as' => 'admin.usuarios', 'uses' => 'UsuariosController@index']);
        Route::get('usuarios/nuevo', ['as' => 'admin.usuarios.nuevo', 'uses' => 'UsuariosController@crearUsuario']);
        Route::get('usuarios/{id}/edit', ['as' => 'admin.usuarios.edit', 'uses' => 'UsuariosController@edit']);

        Route::post('usuarios/store', ['as' => 'admin.usuarios.store', 'uses' => 'UsuariosController@store']);
        Route::put('usuarios/update/{id}', ['as' => 'admin.usuarios.update', 'uses' => 'UsuariosController@update']);


        Route::get('postulantes', ['as' => 'admin.postulantes', 'uses' => 'PostulantesController@index']);
        Route::get('postulantes/{id}/show', ['as' => 'admin.postulantes.show', 'uses' => 'PostulantesController@show']);
        Route::get('postulantes/{id}/destroy', ['as' => 'admin.postulantes.destroy', 'uses' => 'PostulantesController@destroy']);
        Route::get('postulados', ['as' => 'admin.postulados', 'uses' => 'PostuladosController@index']);

        Route::get('empresas/{id}/destroy', ['as' => 'admin.empresas.destroy', 'uses' => 'EmpresasController@destroy']);
        Route::get('emprendedores/{id}/destroy', ['as' => 'admin.emprendedores.destroy', 'uses' => 'EmprendedoresController@destroy']);

        // agregar localidad y provincia a ofertas
        // hacer vista de oferta
        // arreglar msjs de eventos
        // que hacemos con las imagenes??

        // usuarios vistas desde el back


        // armar postularse a oferta!
        // delete de ofertas, empresas y postulantes
        // select dependientes de provincia y localidad


        // posibilidad de adjuntar CV por formulario
        


    });




