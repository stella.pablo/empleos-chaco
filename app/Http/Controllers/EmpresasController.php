<?php

namespace App\Http\Controllers;

use App\Area;
use App\Empresa;
use Illuminate\Http\Request;

use App\Http\Requests;
use Intervention\Image\Facades\Image;

class EmpresasController extends Controller
{

    public function index(){

        $empresas = Empresa::where('deleted_at','=',NULL)->get();

        return view('empresas.index',compact('empresas'));

    }
    public function create(){

        $areas = Area::lists('nombre', 'id');

        return view('empresas.create',compact('areas'));
    }


    public function store(Request $request){

        $v = $this->validate($request, [
            'razon_social' => 'required',
            'area_id' => 'required',
            'direccion' => 'required',
            'email' => 'required|email',
        ]);


        Empresa::cargar($request);

        flash('Empresa nueva!');

        return redirect('admin/empresas');

    }

    public function edit($id){

        $empresa = Empresa::find($id);

        $areas = Area::lists('nombre', 'id');

        return view('empresas.edit',compact('empresa', 'areas'));

    }

    public function update(Request $request, $id){

        $v = $this->validate($request, [
            'razon_social' => 'required',
            'direccion' => 'required',
            'cuit' => 'required',
            'email' => 'required|email',
        ]);


        $empresa = Empresa::findOrFail($id);

        $empresa->fill($request->all());
        $empresa->save();

        flash('Empresa modificada!');

        return redirect('admin/empresas');

    }

    public function destroy($id){

        $dt = new \DateTime();

        Empresa::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Empresa deshabilitada!','danger');

        return redirect('admin/empresas');
    }
}
