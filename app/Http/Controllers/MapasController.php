<?php

namespace App\Http\Controllers;

use App\Avisogps;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

use App\Http\Requests;

class MapasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $puntos = Avisogps::orderBy('created_at','DESC')->get();

        return View('mapas.punto', compact('puntos'));
    }

    public function indexFront(){

        $avisosGps = Avisogps::all();

        return View('mapas.front', compact('avisosGps'));
    }


    public function activar($id)
    {

        Avisogps::find($id)->update(['estado'=>1]);

        flash('Punto de empleo habilitado','success');

        return redirect('admin/geolocalizacion');
    }

    public function destroy($id)
    {
        Avisogps::destroy($id);

        flash('Punto de empleo eliminado','danger');

        return redirect('admin/geolocalizacion');
    }
}
