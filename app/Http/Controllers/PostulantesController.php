<?php

namespace App\Http\Controllers;

use App\Area;
use App\Curso;
use App\Experiencia;
use App\Http\Requests\CursosFormRequest;
use App\Http\Requests\EducacionFormRequest;
use App\Http\Requests\ExperienciaFormRequest;
use App\Nivel;
use App\Localidad;
use App\Oferta;
use App\Postulados;
use App\Provincia;
use App\Postulante;
use App\Educacion;

use App\User;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;

use App\Http\Requests\PostulanteFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Vinicius73\Lavatar\Facade\LavatarFacade;
use Illuminate\Support\Facades\DB;

class PostulantesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','puesto.show']]);
    }


    public function create()
    {

        $localidades = Localidad::lists('nombre', 'id');

        $sexo = ['M' => 'Masculino', 'F' => 'Femenino'];

        $estados = ['Soltero/a' => 'Soltero/a', 'Casado/a' => 'Casado/a', 'Viudo/a' => 'Viudo/a', 'Divorciado/a' => 'Divorciado/a'];

        $usuario = Postulante::where('user_id','=',Auth::user()->id)->first();


        if(count($usuario) > 0){
            

            return view('postulantes.edit', compact('estados', 'sexo', 'localidades','usuario'));

        }else{

            return view('postulantes.create', compact('estados', 'sexo', 'localidades'));

        }


    }

    public function store(PostulanteFormRequest $request)
    {


        $row = Postulante::perfil($request);

        if ($request->hasFile('foto')) {

            $this->fileUpload($request, Auth::user()->id);

        }

        if ($request->hasFile('cv')) {

            $this->fileUploadCV($request, Auth::user()->id);

        }



        flash('Datos de contacto actualizados!','success');

        return redirect('postulantes/create');

    }

    public function fileUpload($request, $id)
    {

        $file = $request->file('foto');

        $imageName = $request->file('foto')->getClientOriginalName();
        \Storage::disk('local')->put($imageName, \File::get($file));

        $post = Postulante::find($id);
        $post->foto = $imageName;
        $post->save();


    }

    public function fileUploadCV($request,$id){

        $file = $request->file('cv');

        $cv = rand(10000,99999).'.'.$request->file('cv')->getClientOriginalExtension();

        Storage::disk('repository')->put($cv, \File::get($file));

        $post = Postulante::where('user_id','=',$id)->first();
        $post->cv = $cv;
        $post->save();


    }

    public function educacion()
    {

        $areas = Area::lists('nombre', 'id');
        $niveles = Nivel::lists('nombre', 'id');

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');

        $listado = $this->getEducacionById();


        $estado = ['En curso' => 'En curso', 'Graduado' => 'Graduado', 'Abandonado' => 'Abandonado'];


        return view('postulantes.educacion', compact('areas', 'localidades', 'estado', 'niveles', 'provincias','listado'));
    }

    public function storeEducacion(EducacionFormRequest $request)
    {

        $row = Educacion::agregar($request);

        return redirect('postulantes/educacion');


    }

    public function getEducacionById(){

        $grid = Educacion::where('postulante_id','=', Auth::user()->id)->get();

        return $grid;

    }

    public function experiencia()
    {

        $areas = Area::lists('nombre', 'id');

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');

        $listado = $this->getExperienciaId();


        return view('postulantes.experiencia', compact('areas', 'localidades', 'estado', 'niveles', 'provincias','listado'));
    }

    public function storeExperiencia(ExperienciaFormRequest $request){


        Experiencia::agregar($request);

        return redirect('postulantes/experiencias_laborales');

    }

    public function getExperienciaId(){

        $grid = Experiencia::where('postulante_id','=', Auth::user()->id)->get();

        return $grid;

    }

    public function cursos(){

        $listado = $this->getCursosId();

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');


        return view('postulantes.cursos',compact('listado','provincias','localidades'));
    }

    public function storeCurso(CursosFormRequest $request)
    {

        Curso::agregar($request);

        flash('Datos actualizados!','success');

        return redirect('postulantes/create');


    }


    public function getCursosId(){

        $grid = Curso::where('postulante_id','=', Auth::user()->id)->get();


        return $grid;

    }


    public function editEducacion($id){

        $educacion = Educacion::find($id);

        $areas = Area::lists('nombre', 'id');
        $niveles = Nivel::lists('nombre', 'id');

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');

        $listado = $this->getEducacionById();


        $estado = ['En curso' => 'En curso', 'Graduado' => 'Graduado', 'Abandonado' => 'Abandonado'];


        return view('postulantes.educacion_edit', compact('educacion','areas', 'localidades', 'estado', 'niveles', 'provincias','listado'));

    }

    public function updateEducacion(Request $request, $id){

        $educacion = Educacion::findOrFail($id);

        $educacion->fill($request->all());
        $educacion->postulante_id = auth()->user()->id;
        $educacion->save();

        flash('Datos actualizados!');

        return redirect('postulantes/educacion');

    }

    public function editExperiencia($id){

        $experiencia = Experiencia::find($id);

        $areas = Area::lists('nombre', 'id');

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');

        $listado = $this->getExperienciaId();


        return view('postulantes.experiencia_edit', compact('experiencia','areas', 'localidades', 'estado', 'niveles', 'provincias','listado'));

    }

    public function updateExperiencia(Request $request, $id){

        $experiencia = Experiencia::findOrFail($id);

        $experiencia->fill($request->all());
        $experiencia->postulante_id = auth()->user()->id;
        $experiencia->save();

        flash('Datos actualizados!','success');

        return redirect('postulantes/experiencias_laborales');

    }

    public function editCurso($id){

        $curso = Curso::find($id);

        $provincias = Provincia::lists('nombre', 'id');
        $localidades = Localidad::lists('nombre', 'id');

        return view('postulantes.cursos_edit',compact('curso','provincias','localidades'));
    }

    public function updateCurso(Request $request, $id){

        $curso = Curso::findOrFail($id);

        $curso->fill($request->all());
        $curso->postulante_id = auth()->user()->id;
        $curso->save();

        flash('Datos actualizados!','success');

        return redirect('postulantes/cursos');

    }


    public function getLogin(){
        return view('auth.login_admin');
    }

    public function index(){

        $postulantes = User::where('deleted_at','=',NULL)
                    ->join('postulantes','postulantes.user_id','=','users.id')
                    ->where('users.rol_id','=',0)
                    ->select('postulantes.nombre','postulantes.apellido','postulantes.fecha_nac','postulantes.dni',
                             'postulantes.direccion','users.email','postulantes.id','users.id as user_id','postulantes.cv')
                    ->orderBy('users.id','DESC')->get();


        return view('postulantes.index',compact('postulantes'));

    }

    public function show($id){

        $postulante = Postulante::find($id);
        
        


        return view('postulantes.show',compact('postulante'));
    }

    public function destroy($id){

       $dt = new \DateTime();

       Postulante::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

       flash('Usuario deshabilitado!','danger');

       return redirect('admin/postulantes');
    }

    public function habilitar($id){

        Postulante::where('id',$id)->update(['deleted_at'=> NULL]);

        flash('Usuario habilitado!','success');

        return redirect('admin/postulantes');
    }

    public function postularse($id){

        $check = Postulados::postular($id);

        if($check == 0){

            flash('Se ha postulado con exito a la oferta laboral, recibira una confirmación por mail para y nos pondremos en contacto con vos!','success');

        }else{

            flash('Ya se postulo a la oferta laboral');

        }
        return back();

    }

    public function badroute(){

        abort(404);
    }





}