<?php

namespace App\Http\Controllers;

use App\Area;
use App\Empresa;
use App\Localidad;
use App\Oferta;
use App\Provincia;
use Illuminate\Http\Request;

use App\Http\Requests\OfertaFormRequest;

class OfertasController extends Controller
{

    public function __construct()
    {
        $this->middleware(['administrador','admin']);

    }

    public function index(){

        $ofertas = Oferta::orderBy('destacar','1')->where('deleted_at', NULL)->orderBy('id','DESC')->get();

        return view('ofertas.index',compact('ofertas'));
    }

    public function create(){

        $areas = Area::lists('nombre', 'id');
        $empresas = Empresa::where('deleted_at','=', NULL)->lists('razon_social','id');


        $localidades = Localidad::lists('nombre','id');
        $provincias = Provincia::lists('nombre','id');


        $modalidades = $this->modalidades();

        return view('ofertas.create',compact('areas','empresas','modalidades','localidades','provincias'));
    }

    public function modalidades(){

        return [''=>'','Full Time'=>'Full Time','Part Time'=>'Part Time','Freelance'=>'Freelance','Contrato'=>'Contrato','Capacitación'=>'Capacitación'];

    }

    public function store(OfertaFormRequest $request){

        Oferta::cargar($request);

        flash('Nueva oferta laboral!','success');

        return redirect('admin/ofertas');
    }

    public function edit($id){
        $oferta = Oferta::find($id);

        $areas = Area::lists('nombre', 'id');
        $empresas = Empresa::where('deleted_at','=', NULL)->lists('razon_social','id');


        $localidades = Localidad::lists('nombre','id');
        $provincias = Provincia::lists('nombre','id');


        $modalidades = $this->modalidades();

        return view('ofertas.edit',compact('oferta','areas','empresas','provincias','modalidades','localidades'));

    }

    public function update(Request $request, $id){

        $oferta = Oferta::findOrFail($id);


        $oferta->fill($request->all());

        if(!isset($request->urgente)){
            $oferta->urgente = 0;
        }

        $oferta->slug = str_slug($request->descripcion_corta);
        $oferta->save();

        flash('Oferta editada!!');

        return redirect('admin/ofertas');

    }

    public function destacar($id){

        $oferta = Oferta::find($id);
        $oferta->destacar = 1;
        $oferta->save();

        flash('Oferta destacada!');

        return redirect('admin/ofertas');

    }

    public function noDestacar($id){

        $oferta = Oferta::find($id);
        $oferta->destacar = NULL;
        $oferta->save();

        flash('Oferta modificada!');

        return redirect('admin/ofertas');

    }

    public function destroy($id){

        $dt = new \DateTime();

        Oferta::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Oferta deshabilitada!','danger');

        return redirect('admin/ofertas');
    }

    public function habilitar($id){

        Oferta::where('id',$id)->update(['deleted_at'=> NULL]);

        flash('Oferta habilitado!','success');

        return redirect('admin/postulantes');
    }

}
