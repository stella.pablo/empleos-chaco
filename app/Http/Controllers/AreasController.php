<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;

use App\Http\Requests;

class AreasController extends Controller
{
    public function index(){

        $areas = Area::all();

        return view('areas.index',compact('areas'));

    }

    public function create(){

        return view('areas.create');

    }

    public function store(Request $request){

        $v = $this->validate($request, [
            'nombre' => 'required',
        ]);


        Area::create($request->all());

        flash('Nueva área!','success');

        return redirect('admin/areas');

    }

    public function edit($id){

        $area = Area::find($id);

        return view('areas.edit',compact('area'));

    }

    public function update(Request $request, $id){


        $v = $this->validate($request, [
            'nombre' => 'required',
        ]);


        $area = Area::findOrFail($id);

        $area->fill($request->all());
        $area->save();

        flash('Area editada!!');

        return redirect('admin/areas');

    }
}
