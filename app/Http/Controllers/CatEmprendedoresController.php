<?php

namespace App\Http\Controllers;

use App\CategoriaEmprendedor;
use Illuminate\Http\Request;

use App\Http\Requests;

class CatEmprendedoresController extends Controller
{
    public function index(){
        $emprendedores = CategoriaEmprendedor::where('deleted_at','=',NULL)->get();


        return view('categorias_emprendimientos.index',compact('emprendedores'));
    }

    public function create(){

        return view('categorias_emprendimientos.create');

    }

    public function store(Request $request){

        $v = $this->validate($request, [
            'descripcion' => 'required|string',
        ]);


       CategoriaEmprendedor::create($request->all());

        flash('Categoria nueva!');

        return redirect('admin/categorias-emprendedores');
    }

    public function edit($id){

        $emprendedor = CategoriaEmprendedor::find($id);

        return view('categorias_emprendimientos.edit',compact('emprendedor'));
    }

    public function update(Request $request, $id){

        $v = $this->validate($request, [
            'descripcion' => 'required|string',
        ]);


        $emprendedor = CategoriaEmprendedor::findOrFail($id);

        $emprendedor->fill($request->all());
        $emprendedor->save();

        flash('Categoria modificada!');

        return redirect('admin/categorias-emprendedores');

    }

    public function destroy($id){

        $dt = new \DateTime();

        CategoriaEmprendedor::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Categoria eliminada!','danger');

        return redirect('admin/categorias-emprendedores');


    }
}
