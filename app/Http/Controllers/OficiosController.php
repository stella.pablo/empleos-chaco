<?php

namespace App\Http\Controllers;

use App\CategoriaOficio;
use App\Oficio;
use Illuminate\Http\Request;
use App\Http\Requests\OficiosFormRequest;

use App\Http\Requests;

class OficiosController extends Controller
{
    public function index(){

        $oficios = Oficio::where('deleted_at','=',NULL)->orderBy('id','DESC')->get();

        return view('oficios.index',compact('oficios'));
    }

    public function create(){

        $categorias = CategoriaOficio::where('deleted_at',NULL)->lists('nombre','id');

        return view('oficios.create',compact('categorias'));
    }

    public function store(OficiosFormRequest $request){

        Oficio::cargar($request);

        flash('Nuevo Oficio!');

        return redirect('admin/oficios');

    }

    public function edit($id){

        $oficio = Oficio::find($id);

        $categorias = CategoriaOficio::where('deleted_at',NULL)->lists('nombre','id');

        return view('oficios.edit',compact('oficio','categorias'));
    }

    public function update(OficiosFormRequest $request, $id){

        Oficio::actualizar($id,$request);

        flash('Oficio modificado!');

        return redirect('admin/oficios');
    }

    public function destroy($id){

        $dt = new \DateTime();

        Oficio::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Oficio deshabilitado!','danger');

        return redirect('admin/oficios');
    }

}
