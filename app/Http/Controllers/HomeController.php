<?php

namespace App\Http\Controllers;

use App\Area;
use App\Avisogps;
use App\Blog;
use App\CategoriaOficio;
use App\Consulta;
use App\Emprendedor;
use App\Empresa;
use App\Galeria;
use App\Http\Requests;
use App\Localidad;
use App\Oferta;
use App\Oficio;
use App\Postulados;
use App\Postulante;
use Illuminate\Http\Request;
use App\Http\Requests\ContactoFormRequest;
use App\Http\Requests\RegistroFormRequest;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ofertas = Oferta::urgente()->ordered()->get();


        $destacado = (new Oferta)->destacado();


        $areas = [''=>'Cualquier área profesional'] +Area::lists('nombre','id')->all();


        $localidades = Localidad::lists('nombre','id');

        $blog = Blog::select('id','slug','titulo','created_at')->take(4)->orderBy('id','DESC')->get();

        return view('sitio.home',compact('ofertas','areas','localidades','destacado','blog'));

    }

    public function contacto(){

        $motivo = [
                     ''=>'Seleccione motivo',
                     'Empresas'=>'Empresas',
                     'Postulantes' => 'Postulantes',
                     'Dudas y consultas' => 'Dudas y consultas',
                    ];

        return view('sitio.contacto', compact('motivo'));
    }

    public function contactoEmpresa(){

        $motivo = [
                     'Búsqueda de personal'=>'Búsqueda de personal',
                    ];

        return view('sitio.contactoEmpresa', compact('motivo'));
    }


    public function getPuesto($slug){

        $oferta = Oferta::where('slug','=',$slug)->first();

        $empresa = Oferta::find($oferta->id)->empresa;

        return view('sitio.puesto',compact('oferta','empresa'));
    }

    public function filtros($request){


        //con area y parametro
        if(($request->area_id != '') and ($request->parametro != ''))  {

            $ofertas = Oferta::where('area_id','=',$request->area_id)
                ->where('descripcion_corta','like', '%'. $request->parametro . '%')
                ->where('localidad_id','=',$request->localidad_id)
                ->get();
        }

        //con area
        if(($request->area_id != '')  and ($request->parametro == '')){

            $ofertas = Oferta::where('area_id','=',$request->area_id)
                ->where('localidad_id','=',$request->localidad_id)
                ->get();

        }

        //con parametro
        if(($request->area_id == '') and ($request->parametro != ''))  {

            $ofertas = Oferta::where('descripcion_corta','like', '%'. $request->parametro . '%')
                ->where('localidad_id','=',$request->localidad_id)
                ->get();
        }

        //sin parametro y area
        if(($request->area_id == '')  and ($request->parametro == '')){

            $ofertas = Oferta::where('localidad_id','=',$request->localidad_id)->get();

        }

        return $ofertas;
    }

    public function busquedasGet(){
        return redirect('/');
    }

    public function busquedas(Request $request){

        $ofertas = $this->filtros($request);

        $areas = [''=>'Cualquier área profesional'] +Area::lists('nombre','id')->all();

        $localidades = Localidad::lists('nombre','id');

        $categorias = Area::getCategoriasConTrabajo();

        $destacado = (new Oferta)->destacado();

        return view('sitio.resultados',compact('ofertas','categorias','areas','localidades','destacado'));
    }

    public function getInfoEmpresa($id){

        $empresa = Empresa::find($id);

        $ofertas = Empresa::find($id)->ofertas;


        return view('sitio.empresa',compact('empresa','ofertas'));
    }

    public function consulta(ContactoFormRequest $request ){

        Consulta::create($request->all());

        flash('Consulta enviada con exito!','success');

        return redirect('contacto');

    }

    public function getConsultas(){

        $consultas = Consulta::orderBy('created_at','DESC')->get();

        return view('consultas.index',compact('consultas'));

    }

    public function emprendedores(){

        $emprendedores = Emprendedor::orderBy('id', 'DESC')->where('deleted_at',NULL)->paginate(10);

        return view('sitio.emprendedores', compact('emprendedores'));
    }

    public function lideres(){

        return view('joveneslideres.index');
    }

    public function financiamiento(){

        return view('joveneslideres.financiamiento');
    }

    public function agenda(){

        return view('joveneslideres.agenda');
    }

    public function inscripcion(){

        return view('joveneslideres.inscripcion');
    }

    public function contacto2(){

        return view('joveneslideres.contactenos');
    }


    public function oficios(){

        $categorias = Area::getCategoriasConTrabajo();

        $oficios = Oficio::join('categorias_oficios','categorias_oficios.id','=','oficios.categoria_oficio_id')
            ->where('oficios.deleted_at','=',NULL)
            ->select('oficios.id','oficios.responsable','oficios.direccion','oficios.telefono','oficios.email','categorias_oficios.nombre as oficio')
            ->orderBy('oficios.id','DESC')
            ->orderBy('categoria_oficio_id', 'ASC')->paginate(20);
        

        return view('sitio.oficios_2', compact('oficios'));
    }

    public function emprendimiento($slug){

        $emprendimiento = Emprendedor::where('slug',$slug)->first();


        $galeria = Galeria::where('emprendedor_id','=',$emprendimiento->id)->get();


        return view('sitio.emprendimiento',compact('emprendimiento','galeria'));
    }

    public function buscarEmprendimientos(Request $request){

        //dd($request->all());

        $emprendedores= DB::table('emprendedores')
            ->join('categorias_emprendedores','categorias_emprendedores.id','=','emprendedores.categoria_emprendedor_id')
            ->where('categorias_emprendedores.descripcion','like', '%'. $request->parametro . '%')
            ->orWhere('emprendedores.nombre','like', '%'. $request->parametro . '%')
            ->where('emprendedores.deleted_at',NULL)
            ->paginate(20);

        return view('sitio.emprendedores',compact('emprendedores'));

    }

    public function buscarOficios(Request $request){

        $oficios = Oficio::join('categorias_oficios','categorias_oficios.id','=','oficios.categoria_oficio_id')
            ->select('oficios.responsable','oficios.direccion','oficios.telefono','oficios.email','categorias_oficios.nombre as oficio')
            ->where('categorias_oficios.nombre','like', '%'. $request->parametro . '%')
            ->orWhere('oficios.responsable','like', '%'. $request->parametro . '%')
            ->get();

        $json = $oficios->toJson();

        return view('sitio.oficios',compact('json'));

    }

    public function dashboard(){

        $postulantes = Postulante::count();

        $postulados = Postulados::count();

        $ofertas = Oferta::count();

        return view('ofertas.dashboard',compact('postulantes','postulados','ofertas'));

    }

    public function registrarMapa(){
        return view('sitio.registro');
    }

    public function saveGeolocalizacion(RegistroFormRequest $request){

        Avisogps::create($request->all());

        flash('Puesto de trabajo registrado con exito, se verificara su autenticidad antes de publicarlo. Muchas gracias','success');

        return redirect('registro');


    }

    public function mapaGeolocalizacion()
    {

        $avisosGps = Avisogps::where('estado','=',1)->get();

        return View('mapas.index', compact('avisosGps'));
    }

    public function getOficio($id){

        $oficio = Oficio::findOrFail($id);
        
        if($oficio->rating == 0){
            
        }

        return view('sitio.oficio',compact('oficio'));

    }

    public function rating(Request $request){

        $rating = Oficio::find($request->id);

        $rating->cantidad = $rating->cantidad + 1;
        $star = $rating->rating + $request->star;

        $rating->rating = $star / $rating->cantidad;

        $rating->save();

    }
}
