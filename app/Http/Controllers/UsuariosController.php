<?php

namespace App\Http\Controllers;

use App\Consulta;
use App\Rol;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    public function getLogin(){

        if (isset(auth()->user()->rol_id)){
            return redirect()->intended('admin/dashboard');
        }

        return view('auth.login_admin');
    }


    public function authenticate(Request $request)
    {
        $this->validar($request);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'rol_id'=> 3]))
        {
            return redirect('admin/dashboard');

        }else{

            flash('Acceso Incorrecto','danger');

            return redirect('gestion/login');

        }
    }

    protected function validar($request){
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

    }

    public function crearUsuario(){

        $roles = ['1'=>'Oficios','2'=>'Emprendedores','3'=>'Administrador'];

        return view('usuarios.create',compact('roles'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);


        User::create([
            'nombre' => $request->nombre,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'rol_id' => 3,
            'nivel' => $request->nivel
        ]);

        flash('Nuevo usuario!');

        return redirect('admin/usuarios');
    }

    public function index(){

        $usuarios = User::join('roles','users.rol_id','=','roles.id')
            ->select('users.nombre','users.email','roles.nombre as rol','users.id')
            ->orderBy('roles.id','DESC')
            ->get();


        return view('usuarios.index',compact('usuarios'));
    }

    public function edit($id){

        $usuario = User::find($id);

        $roles = Rol::lists('nombre','id');

        return view('usuarios.edit',compact('usuario','roles'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::findOrFail($id);

        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->save();


        flash('Usuario modificado!');

        return redirect('admin/usuarios');

    }

    public function getConsultas($id){

        $consulta = Consulta::find($id);

        return view('consultas.show',compact('consulta'));

    }
}
