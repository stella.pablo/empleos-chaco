<?php

namespace App\Http\Controllers;

use App\CategoriaEmprendedor;
use App\Emprendedor;
use App\Galeria;
use Illuminate\Http\Request;
use App\Http\Requests\EmprendedoresFormRequest;

use App\Http\Requests;

class EmprendedoresController extends Controller
{
    public function index(){

        $emprendedores = Emprendedor::where('deleted_at',NULL)->orderBy('id','DESC')->get();

        return view('emprendedores.index',compact('emprendedores'));

    }

    public function create(){

        $categorias = CategoriaEmprendedor::where('deleted_at','=',NULL)->lists('descripcion','id');

        return view('emprendedores.create',compact('categorias'));

    }

    public function store(EmprendedoresFormRequest $request){

        $id = Emprendedor::cargar($request);

        if($request->galeria != NULL){

            $this->saveGaleria($request,$id);

        }

        flash('Nuevo Emprendimiento!');

        return redirect('admin/emprendedores');
    }

    public function saveGaleria($request,$id){

            Galeria::guardar($request->galeria,$id);

    }

    public function edit($id){

        $emprendedor = Emprendedor::find($id);
        $categorias = CategoriaEmprendedor::lists('descripcion','id');

        $galeria = Galeria::where('emprendedor_id',$id)->get();


        return view('emprendedores.edit',compact('emprendedor','categorias','galeria'));
    }

    public function update(EmprendedoresFormRequest $request, $id){


        Emprendedor::actualizar($id,$request);


        if($request->galeria[0] != NULL){

            $this->saveGaleria($request,$id);

        }

        flash('Emprendimiento actualizado!');

        return redirect('admin/emprendedores');
    }

    public function destroy($id){

        $dt = new \DateTime();

        Emprendedor::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Emprendimiento deshabilitado!','danger');

        return redirect('admin/emprendedores');
    }

    public function eliminarImagen($id,$emp){

        Galeria::destroy($id);

        flash('Imagen eliminada!','danger');

        return redirect()->route('admin.emprendedores.edit', $emp);

    }


}
