<?php

namespace App\Http\Controllers;

use App\CategoriaOficio;
use Illuminate\Http\Request;

use App\Http\Requests;

class CatOficiosController extends Controller
{
    public function index(){
        $oficios = CategoriaOficio::where('deleted_at','=',NULL)->get();

        return view('categorias_oficios.index',compact('oficios'));
    }

    public function create(){

        return view('categorias_oficios.create');

    }

    public function store(Request $request){

        $v = $this->validate($request, [
            'nombre' => 'required|string',
        ]);


        CategoriaOficio::create($request->all());

        flash('Categoria nueva!');

        return redirect('admin/categorias-oficios');
    }

    public function edit($id){

        $oficio = CategoriaOficio::find($id);

        return view('categorias_oficios.edit',compact('oficio'));
    }

    public function update(Request $request, $id){

        $v = $this->validate($request, [
            'nombre' => 'required|string',
        ]);


        $emprendedor = CategoriaOficio::findOrFail($id);

        $emprendedor->fill($request->all());
        $emprendedor->save();

        flash('Categoria modificada!');

        return redirect('admin/categorias-oficios');

    }

    public function destroy($id){

        $dt = new \DateTime();

        CategoriaOficio::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Categoria eliminada!','danger');

        return redirect('admin/categorias-oficios');

    }
}
