<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Requests\BlogFormRequest;

use App\Http\Requests;

class BlogController extends Controller
{
    public function index(){

        $blog = Blog::where('deleted_at','=',NULL)->orderBy('id','DESC')->get();


        return view('blog.index',compact('blog'));
    }


    public function create(){

        $estado = ['1' => 'Publicado'];
        return view('blog.create',compact('estado'));
    }

    public function store(BlogFormRequest $request){

       $blog = (new Blog())->guardar($request);

        flash('Nuevo articulo!');

        return redirect('admin/blog');
        
    }

    public function get(){

        $blog = Blog::where('deleted_at','=',NULL)->orderBy('id','DESC')->paginate(6);


        return view('blog.show',compact('blog'));
    }

    public function getPost($slug){

        $blog = Blog::where('slug','=',$slug)->first();

        return view('blog.post',compact('blog'));

    }

    public function edit($id){
        $blog = Blog::find($id);

        $estado = ['1' => 'Publicado'];

        return view('blog.edit',compact('blog','estado'));
    }
    
    public function update(BlogFormRequest $request, $id){
        
        $blog = (new Blog())->actualizar($request,$id);

        flash('Articulo actualizado!');

        return redirect('admin/blog');
    }

    public function destroy($id){

        $dt = new \DateTime();

        Blog::where('id',$id)->update(['deleted_at'=>$dt->format('Y-m-d H:i:s')]);

        flash('Articulo deshabilitado!','danger');

        return redirect('admin/blog');
    }


}
