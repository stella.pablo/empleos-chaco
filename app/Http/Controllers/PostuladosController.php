<?php

namespace App\Http\Controllers;

use App\Postulados;
use Illuminate\Http\Request;

use App\Http\Requests;

class PostuladosController extends Controller
{
    public function index(){

        $postulados = (new Postulados)->listado();

        return view('postulados.index',compact('postulados'));
    }
}
