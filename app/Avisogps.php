<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avisogps extends Model
{
    protected $fillable = ['id', 'nombre', 'cuit', 'direccion', 'latitud', 'longitud', 'cantpuestos', 'puesto',
                            'detalles', 'categoria', 'estado'];

}
