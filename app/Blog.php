<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = ['titulo','cuerpo','estado_id','imagen','slug','bajada'];


    public function guardar($request){

        $nombre = $this->cargarImagen($request);

        Blog::create(['titulo'=>$request->titulo,
            'cuerpo'=>$request->cuerpo,
            'bajada'=>$request->bajada,
            'estado_id'=>$request->estado_id,
            'imagen'=> $nombre,
            'slug'=>str_slug($request->titulo),
        ]);
    }


    public function actualizar($request,$id){

        $nombre = $this->cargarImagen($request);

        $check = Blog::find($id);

        if($nombre == NULL){
                $nombre = $check->imagen;
        }

        Blog::find($id)->update(['titulo'=>$request->titulo,
            'cuerpo'=>$request->cuerpo,
            'bajada'=>$request->bajada,
            'estado_id'=>$request->estado_id,
            'imagen'=>$nombre,
            'slug'=>str_slug($request->titulo),
        ]);

    }

    public static function cargarImagen($request){

        $nombre = NULL;

        if($request->imagen){

            $nombre =  rand(10000,99999).'.'.$request->imagen->getClientOriginalExtension();

            Image::make($request->imagen)->save('images/articulos/'. $nombre);
            Image::make($request->imagen)->resize(360,240)->save('images/articulos/thumb/'. $nombre);

        }

        return $nombre;

    }
}
