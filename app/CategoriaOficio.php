<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaOficio extends Model
{
    protected $table = 'categorias_oficios';

    protected $fillable = ['nombre'];
}
