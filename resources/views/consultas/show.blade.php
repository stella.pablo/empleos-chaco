@extends('template.admin_template')

@section('titulo')
    Ver Consultas
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Consultas y mensajes</a></li>
        <li class="active">Nueva</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Nombre</label>
                        <div class="col-sm-7">
                            <p>{{ $consulta->nombre }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-7">
                            <p>{{ $consulta->email }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Motivo</label>
                        <div class="col-sm-7">
                            <p>{{ $consulta->motivo }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Consulta</label>
                        <div class="col-sm-12">
                            {!! Form::textarea('descripcion_larga', $consulta->consulta, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
    </div>

 @endsection
