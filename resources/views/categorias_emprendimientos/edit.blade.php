@extends('template.admin_template')

@section('titulo')
    Categoria de Emprendimientos
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Emprendimientos</a></li>
        <li class="active">Editar</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($emprendedor, array('method' => 'PUT', 'route' => array('admin.categorias-emprendedores.update', $emprendedor->id), 'class' => 'form-horizontal')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-8">
                            {!! Form::text('descripcion', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
@endsection