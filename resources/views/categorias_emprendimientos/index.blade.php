@extends('template.admin_template')

@section('titulo')
    Categorias de Emprendedores
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Categorias de Emprendedores</a></li>
        <li class="active">Grid</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div class="col-md-2">
                        <a href="{{ route('admin.categorias-emprendedores.create') }}" class="btn btn-block btn-success">Nuevo</a>
                    </div>
                    </br>
                    <div id="bodyContenido" >
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th></th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <div class="col-md-8">
                                @include('layouts.flash')
                            </div>
                            @foreach($emprendedores as $row)
                                <tr>
                                    <td>{{ $row->descripcion }}</td>
                                    <td>{{ $row->icono }}</td>
                                    <td>
                                        <a href="{{ route('admin.categorias-emprendedores.edit',$row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-fw fa-edit"></i></a>
                                        <a href="{{ route('admin.categorias-emprendedores.delete',$row->id) }}" onclick="return confirm('Seguro que desea eliminar la categoria ?')" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
    @endsection
    @section('recursos')
            <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                }
            });
        });
    </script>

@endsection