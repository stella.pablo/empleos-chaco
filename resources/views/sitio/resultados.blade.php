@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <!--CATEGORIAS POPULARES -->

    <section class="popular-job-caregries popular-categories candidates-listing">

        <div class="holder">

            <div class="container">

                <h4>Ofertas por áreas</h4>

                <div id="popular-job-slider" class="owl-carousel owl-theme">

                    @foreach($categorias as $row)
                        <div class="item">
                            <div class="box"> <img alt="img" src="{{ url('images/categories-icon-'.$row->icono.'.png') }}">
                                <h4><a href="#">{{ $row->nombre }}</a></h4>
                                <strong>{{ $row->total }} puestos disponibles</strong>
                            </div>
                    </div>
                    @endforeach

                </div>

            </div>

        </div>

    </section>
    <!--CATEGORIAS POLULARES-->

    <!--MAIN -->
    <div id="main">
        <!--SEARCH BAR SECTION -->
        <section class="candidates-search-bar">
            <div class="container">
                {!! Form::open(array('route' => 'busquedas')) !!}

                <div class="container">

                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <input type="text" name="parametro" placeholder="Escribe cargo o área profesional">
                        </div>

                        <div class="col-md-3 col-sm-3">
                            {!! Form::select('localidad_id', $localidades ,  null, ['class' => 'select_busqueda'])!!}
                        </div>

                        <div class="col-md-4 col-sm-4">
                            {!! Form::select('area_id',  $areas ,  null, ['class' => 'select_busqueda'])!!}
                        </div>

                        <div class="col-md-1 col-sm-1">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>

        </section>

        <!--SEARCH BAR SECTION END-->

        <!--RECENT JOB SECTION START-->

        <section class="recent-row padd-tb">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <div id="content-area">
                            @if(count($ofertas) > 0)
                                <h2>Resultados</h2>
                                <ul id="myList">
                                    @foreach($ofertas as $row)
                                        <li>
                                            <div class="box">
                                                <div class="text-col">
                                                    <h4><a href="{{ route('puesto.show',$row->slug) }}">{{ $row->descripcion_corta }}</a></h4>
                                                    <p>{{ \App\Area::find($row->area_id)->nombre }}</p>
                                                    <p>{{ \App\Empresa::find($row->empresa_id)->razon_social }}</p>
                                                    <a href="#" class="text"><i class="fa fa-map-marker"></i>{{ getUbicacion($row->provincia_id,$row->localidad_id) }}</a> <a href="#" class="text"><i class="fa fa-calendar"></i>{{ $row->fecha_inicio->format('d/m/Y') }}</a>
                                                </div>
                                                <a href="#" class="btn-1 btn-color-{{ getColor($row->modalidad) }} ripple">{{ $row->modalidad }}</a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <h2>Sin Resultados</h2>

                            @endif

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h2>Destacados</h2>
                        <aside>
                            <div class="sidebar">
                                @foreach($destacado as $row)
                                    <div class="box">
                                        <div class="text-box">
                                            <a href="#" class="btn-time">{{ $row->modalidad }}</a>
                                            <h4><a href="{{ route('puesto.show',$row->slug) }}">{{ $row->descripcion_corta }}</a></h4>
                                            <a href="#" class="text"><i class="fa fa-map-marker"></i>{{ getUbicacion($row->provincia_id,$row->localidad_id) }}</a> <a href="#" class="text"><i class="fa fa-calendar"></i>{{ $row->fecha_inicio->format('d/m/Y') }}</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

        </section>

        <!--RECENT JOB SECTION END-->

    </div>

    <!--MAIN END-->



@endsection