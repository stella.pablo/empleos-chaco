@extends('layouts.master')
@section('style')
<link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

<!--INNER BANNER START-->
<section id="inner-banner">
    <div class="container">
        <h1>Comunícate con la Secretaría de Empleo y Trabajo</h1>
    </div>
</section>
<!--INNER BANNER END-->


<!--MAIN START-->
<div id="main">
    <section class="contact-section">
        <div class="map-box">
            <div class="contact-form padd-tb">
                <div class="container">
                    <div id="content-area">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <h2>Formulario de Contacto</h2>
                                <div class="box">                                
                                    @include('layouts.flash_front')
                                    @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Te estás olvidando de algo</h4>
                                    </br>
                                    @foreach ( $errors->all() as $error )
                                    <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                                @endif

                                {!! Form::open(array('route' => 'consulta.store', 'class' => 'form-horizontal')) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! Form::text('nombre',null,['placeholder'=>'Nombre'])!!}
                                    </div>

                                    <div class="col-md-12">
                                        {!! Form::text('email',null,['placeholder'=>'Email'])!!}
                                    </div>

                                    <div class="col-md-12">
                                        {!! Form::select('motivo',  $motivo ,  null)!!}
                                    </div>

                                    <div class="col-md-12">
                                        <textarea name="consulta" cols="10" rows="10" placeholder="Mensaje"></textarea>
                                    </div>

                                    <div class="col-md-12">
                                        <input name="comments" type="submit" value="Consultar">
                                    </div>
                                </div>
                                {!! Form::close() !!}


                            </div>

                        </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="box">
                                        @include('sitio.contactoAutoridades')
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</div>

@endsection