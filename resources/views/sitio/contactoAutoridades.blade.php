<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
                <div class="col-sm-4"><img src="{{ url('images/logo_secretaria.png') }}" alt="" width="100%"></div>
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    <p align="center">
                        <strong>Liliana Spoljaric</strong> <br>
                        Secretaría de Empleo y Trabajo</p>
                    </div>
                </div>
                <img src="{{ url('images/linea_azul.png') }}" alt="" width="100%">
                <table class="table table-striped">
                    <tr>
                        <td width="10%" style="vertical-align:middle">
                            <img src="{{ url('images/logo_facebook.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="https://www.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547/?fref=ts" target="_new">
                                <strong>Secretaria de Empleo y Trabajo</strong>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                  <div class="col-sm-4"><img src="{{ url('images/logo_empleo.png') }}" alt="" width="100%"></div>
                  <div class="col-sm-2"></div>
                  <div class="col-sm-6">
                      <p align="center">
                          <strong>María Natalia Mustillo</strong> <br>
                          Subsecretaria de Empleo</p>
                      </div>
                  </div>
                  <img src="{{ url('images/linea_azul.png') }}" alt="" width="100%">
                  <table class="table table-striped">
                    <tr>
                        <td width="10%" style="vertical-align:middle">
                            <img src="{{ url('images/logo_facebook.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="https://www.facebook.com/subsecretariaempleodechaco" target="_new">
                                <strong>SubsecretariaEmpleoDeChaco</strong>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:middle">
                            <img src="{{ url('images/logo_twitter.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="https://twitter.com/SsDeEmpleoChaco" target="_new">
                                <strong>@SsDeEmpleoChaco</strong>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:middle">
                            <img src="{{ url('images/logo_mail.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="mailto:subsec.empleochaco@gmail.com">
                                <strong>subsec.empleochaco@gmail.com</strong>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:middle">
                            <img src="{{ url('images/logo_telefono.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="tel:+5493624979134"><strong>(3624)979134</strong></a>
                            <br>
                            <a href="tel:+5493624979135"><strong>(3624)979135</strong></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:middle">
                            <img src="{{ url('images/logo_direccion.png') }}" alt="" width="100%">
                        </td>
                        <td style="vertical-align:middle">
                            <a href="https://www.google.com.ar/maps/place/Av.+Moreno+77,+H3500BAN+Resistencia,+Chaco/@-27.4543524,-58.9937516,17z/data=!3m1!4b1!4m5!3m4!1s0x94450c8c9359815d:0x724dccab23b521c0!8m2!3d-27.4543524!4d-58.9915629" target="_new"><strong>Av. Sarmiento y Marcelo T. de Alvear, Resistencia, Chaco</strong></a>
                        </td>
                    </tr>
                </table>
            </div>
</div>
