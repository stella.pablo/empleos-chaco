@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ url('plugin/css/chocolat.css') }}" type="text/css" media="screen" charset="utf-8">
@endsection
@section('content')

    <!--JOB DETAIL-->
    <section class="recent-row padd-tb job-detail">

        <div class="container">
            <div class="row">

                @include('layouts.flash_front')

                <div class="col-md-9 col-sm-8">
                    <div id="content-area">
                        <div class="box">
                            <div class="thumb">
                                <img src="{{ url('images/emprendedores/'. $emprendimiento->logo) }}" width="165px" alt="img">
                            </div>

                            <div class="text-col">
                                <h2><a href="#">{{ $emprendimiento->nombre }}</a></h2>
                                <h4>{{ \App\CategoriaEmprendedor::find($emprendimiento->categoria_emprendedor_id)->nombre }}</h4>
                                <p>{{ $emprendimiento->responsable }}</p>
                                <a href="#" class="text"><i class="fa fa-map-marker"></i>{{ $emprendimiento->direccion }}</a>
                                <a href="#" class="text"><i class="fa fa-mail-reply "></i>{{ $emprendimiento->email }}</a>
                                <a href="#" class="text"><i class="fa fa-street-view "></i>{{ $emprendimiento->direccion }}</a>
                            </div>

                            <div class="clearfix">
                                <p>{!! $emprendimiento->descripcion !!}</p>
                            </div>

                            @if(count($galeria) > 0)
                                <div class="clearfix">
                                    <div class="chocolat-parent" data-chocolat-title="{{ $emprendimiento->nombre }}">
                                        @foreach($galeria as $row)
                                            <a class="chocolat-image" href="{{ url('images/galeria/'. $row->nombre) }}" >
                                                <img width="30%"  src="{{ url('images/galeria/'. $row->nombre) }}" />
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <div class="sidebar">
                            <div class="sidebar-jobs">
                                <ul>
                                    <li>
                                        <a href="{{ action('HomeController@contacto') }}">
                                            <img src="{{ url('images/logo_es.png') }}" alt="" style="width: 100%" >
                                        </a>
                                    </li>
                                    <li style="text-align: center"> <strong>Katia Blanc</strong> <br>
                                        Subsecretaria de Economía Social
                                    </li>
                                    <li>
                                        <table class="table table-striped">
                                            <tr>
                                                <td width="20%" style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_facebook.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://www.facebook.com/economiasocialchaco" target="_new">
                                                        <strong>EconomiaSocialChaco</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_twitter.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://twitter.com/economiasocialchaco" target="_new">
                                                        <strong>@economiaSocialChaco</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_telefono.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="tel:+5493624446996"><strong>(3624)446996</strong></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_direccion.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://www.google.com.ar/maps/place/Jos%C3%A9+Mar%C3%ADa+Paz+1620,+H3506EUM+Resistencia,+Chaco/@-27.4657009,-59.0014297,17z/data=!3m1!4b1!4m5!3m4!1s0x94450c90b554b7f1:0xf4456424e0c70f13!8m2!3d-27.4657009!4d-58.999241" target="_new"><strong>Av. Sarmiento y Marcelo T. de Alvear, Resistencia, Chaco</strong></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!--JOB DETAIL-->
    @section('script')
        <script type="text/javascript" src="{{ url('plugin/js/jquery.chocolat.js') }}"></script>

        <script>
            $(document).ready(function(){
                $('.chocolat-parent').Chocolat();
            });
        </script>
    @endsection

@endsection