@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <!--INNER BANNER START-->

    <section id="inner-banner">

        <div class="container">

            <h1>Detalle del puesto</h1>

        </div>

    </section>

    <!--INNER BANNER END-->

    <!--JOB DETAIL-->

    <section class="recent-row padd-tb job-detail">

        <div class="container">

            <div class="row">

                @include('layouts.flash_front')

                <div class="col-md-9 col-sm-8">

                    <div id="content-area">
                        <div class="box">
                            <div class="text-col">

                                <h2><a href="#">{{ $oferta->descripcion_corta }}</a></h2>
                                <p>{{ $empresa->razon_social }} <em>(Ver todos los puestos)</em></p>
                                <p>Cantidad de puestos: {{ $oferta->puestos }}</p>
                                <a href="#" class="text">
                                    <i class="fa fa-map-marker"></i>Chaco, Resistencia
                                </a>
                                @if($oferta->fecha_inicio != NULL)
                                    <a href="#" class="text">
                                        <i class="fa fa-calendar"></i>{{ $oferta->fecha_inicio->format('d/m/Y') }}
                                    </a>
                                @endif
                                <div class="clearfix">
                                    <a href="#" class="btn-freelance">{{ $oferta->modalidad }}</a>
                                    @if(auth()->check())
                                        <a href="{{ url('postularse', $oferta->id) }}" class="btn-style-1">Postularse a este trabajo</a>
                                    @else
                                        <a href="{{ url('login') }}" class="btn-style-1">Postularse a este trabajo</a>
                                    @endif
                                </div>

                            </div>

                            <div class="clearfix">

                                <h4>Visión</h4>
                                <p>{{ $empresa->sinopsis }}</p>
                                <h4>Requerimientos del puesto</h4>

                                <ul>
                                    <li>
                                        {!! $oferta->descripcion_larga !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-4">
                    <aside>
                        <div class="sidebar">
                            <div class="box">
                                <div class="thumb">
                                        @if($empresa->logo != NULL)
                                        <a href="{{ route('empresa.show',$empresa->id) }}"><img src="{{ url('images/empresas/'.$empresa->logo) }}" alt="img"></a>
                                        @endif
                                </div>
                                <div class="text-box">
                                    <h4><a href="{{ route('empresa.show',$empresa->id) }}">{{ $empresa->razon_social }}</a></h4>
                                    <p>{{ str_limit($empresa->sinopsis,100) }}...</p>
                                    <strong>{{ \App\Area::find($empresa->area_id)->nombre }}</strong>
                                    <strong>Ubicación</strong>
                                    <p>{{ $empresa->direccion }}</p>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <!--JOB DETAIL-->

@endsection