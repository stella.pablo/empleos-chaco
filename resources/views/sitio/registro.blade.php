@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')


    <section id="inner-banner">

        <div class="container">
            <h1>Búsqueda de personal</h1>
        </div>

    </section>

    <section class="resum-form padd-tb">

        <div class="container">

            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>


            {!! Form::open(array('route' => 'store.geolocalizacion', 'class' => 'form-horizontal')) !!}


            @include('layouts.flash_front')

            <div class="row">
                <h3>Datos del negocio</h3>
                <hr>
                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-5 control-label">Nombre del negocio</label>
                    <div class="col-sm-7">
                        {!! Form::text('nombre')!!}
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-3 control-label">CUIT Nro</label>
                    <div class="col-sm-6">
                        {!! Form::text('cuit')!!}
                    </div>
                </div>


                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-5 control-label">Dirección</label>
                    <div class="col-sm-6">
                        {!! Form::text('direccion')!!}
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-3 control-label">Localidad</label>
                    <div class="col-sm-6">
                        {!! Form::text('localidad')!!}
                    </div>
                </div>

                <h3>Sobre el empleo</h3>
                <hr>

                <div class="col-md-6 col-sm-6">

                    <label class="col-sm-5 control-label">Latitud</label>
                    <div class="col-sm-4">
                        {!! Form::text('latitud',null,['placeholder'=>'-00.00000'])!!}
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-3 control-label">Longitud</label>
                    <div class="col-sm-4">
                        {!! Form::text('longitud',null,['placeholder'=>'-00.00000'])!!}
                    </div>
                    <span style="color: #880000; font-style: italic; font-size: 12px;">Cómo puedo saber cual es la latitud y longitud de mi negocio? (Ver indicaciones).</span>
                </div>


                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-5 control-label">Puesto</label>
                    <div class="col-sm-6">
                        {!! Form::text('puesto')!!}
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <label class="col-sm-3 control-label">Cantidad</label>
                    <div class="col-sm-4">
                        {!! Form::text('cantpuestos')!!}
                    </div>
                </div>

                <div class="col-md-10 col-sm-10">
                    <label class="col-sm-3 control-label">Descripción del puesto</label>
                    <div class="col-sm-9">
                        {!! Form::textarea('detalles')!!}
                    </div>
                </div>


            </div>


            <div class="col-lg-offset-8">
                <input style="margin: 3px" type="submit" class="guardar" value="Registrar">
            </div>

            {{ Form::close() }}

        </div>
        </div>

        </div>

    </section>

@endsection
@section('script')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2016'
            });
        });
    </script>

@endsection
