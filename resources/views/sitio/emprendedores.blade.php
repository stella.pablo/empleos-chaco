@extends('layouts.master')
@section('style')
<link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div id="wrapper">


   <div id="main">

       <!--INNER BANNER START-->
       <section id="inner-banner">
           <div class="container">
               <h1>Registro de Emprendedores</h1>
           </div>
       </section>
       <!--INNER BANNER END-->

       <!--SEARCH BAR SECTION -->
       <section class="candidates-search-bar">
           <div class="container">
               {!! Form::open(array('route' => 'busquedas.emprendimientos')) !!}

               <div class="container">

                   <div class="row">
                       <div class="col-md-10 col-sm-10">
                           <input type="text" name="parametro" placeholder="Escriba nombre del emprendimiento que desea buscar">
                       </div>

                       <div class="col-md-1 col-sm-1">
                           <button type="submit"><i class="fa fa-search"></i></button>
                       </div>
                   </div>
               </div>

               {{ Form::close() }}
           </div>
       </section>
       <!--SEARCH BAR SECTION END-->


       <!--RECENT JOB SECTION START-->
       <section class="resumes-section padd-tb">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="resumes-content">
                        @foreach($emprendedores as $row)
                        <div class="box">
                            <div class="frame">
                                <a href="{{ route('guia.emprendimiento',$row->slug) }} "><img src="{{ url('images/emprendedores/'. $row->logo) }}" width="165px" alt="img"></a></div>

                                <div class="text-box">
                                    <h2><a href="{{ route('guia.emprendimiento',$row->slug) }}">{{ $row->nombre }}</a></h2>
                                    <h4>Categoria: {{ \App\CategoriaEmprendedor::find($row->categoria_emprendedor_id)->descripcion }}</h4>
                                    <div class="clearfix">
                                        <strong><i class="fa fa-street-view "></i>{{ $row->direccion }}</strong>
                                        <strong><i class="fa fa-phone "></i>{{ $row->telefono }}</strong>
                                        <strong><i class="fa fa-globe"></i><a href="#">{{ $row->email }}</a></strong> </div>
                                        <div class="btn-row">
                                            <a href="{{ route('guia.emprendimiento',$row->slug) }}" class="contact">Contactar</a>
                                        </div>
                                    </div>

                                </div>
                                @endforeach

                                {{ $emprendedores->links() }}
                            </div>
                        </div>
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <div class="sidebar">
                            <div class="sidebar-jobs">
                                <ul>
                                    <li>
                                        <a href="{{ action('HomeController@contacto') }}">
                                            <img src="{{ url('images/logo_es.png') }}" alt="" style="width: 100%" >
                                        </a>
                                    </li>
                                    <li style="text-align: center"> <strong>Katia Blanc</strong> <br>
                                        Subsecretaria de Economía Social
                                    </li>
                                    <li>
                                        <table class="table table-striped">
                                            <tr>
                                                <td width="20%" style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_facebook.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://www.facebook.com/economiasocialchaco" target="_new">
                                                        <strong>EconomiaSocialChaco</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_twitter.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://twitter.com/economiasocialchaco" target="_new">
                                                        <strong>@economiaSocialChaco</strong>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_telefono.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="tel:+5493624446996"><strong>(3624)446996</strong></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:middle">
                                                    <img src="{{ url('images/logo_direccion.png') }}" alt="" width="100%">
                                                </td>
                                                <td style="vertical-align:middle">
                                                    <a href="https://www.google.com.ar/maps/place/Jos%C3%A9+Mar%C3%ADa+Paz+1620,+H3506EUM+Resistencia,+Chaco/@-27.4657009,-59.0014297,17z/data=!3m1!4b1!4m5!3m4!1s0x94450c90b554b7f1:0xf4456424e0c70f13!8m2!3d-27.4657009!4d-58.999241" target="_new"><strong>Av. Sarmiento y Marcelo T. de Alvear, Resistencia, Chaco</strong></a>
                                                </td>
                                            </tr>
                                        </table>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
       </section>
       <!--RECENT JOB SECTION END-->

        </div>

    </div>



    @endsection