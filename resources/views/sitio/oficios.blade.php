@extends('layouts.master')
@section('style')
<link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ url('css/rating.min.css') }}">

@endsection
@section('content')

<div id="wrapper">


    <div id="main">
        <!--INNER BANNER START-->
        <section id="inner-banner">
            <div class="container">
                <h1>Registro de Oficios</h1>
            </div>

        </section>

        <!--INNER BANNER END-->
        <!--MAIN -->
        <div id="main">
                <!--SEARCH BAR SECTION -->
                <section class="candidates-search-bar">
                    <div class="container">
                        {!! Form::open(array('route' => 'busquedas.oficios')) !!}

                        <div class="container">

                            <div class="row">
                                <div class="col-md-10 col-sm-10">
                                    <input type="text" name="parametro" placeholder="Escriba nombre del oficio que desea buscar">
                                </div>

                                <div class="col-md-1 col-sm-1">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>

                        {{ Form::close() }}
                    </div>

                </section>
                <!--SEARCH BAR SECTION END-->
                <section class="resumes-section padd-tb">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-8">
                                <div class="resumes-content">
                                            <div id="shop"></div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4">
                            <aside>
                                <div class="sidebar">
                                    <div class="sidebar-jobs">
                                        <ul>
                                            <li>
                                                <a href="{{ action('HomeController@contacto') }}">
                                                    <img src="{{ url('images/logo_es.png') }}" alt="" style="width: 100%" >
                                                </a>
                                            </li>
                                            <li style="text-align: center"> <strong>Katia Blanc</strong> <br>
                                                Subsecretaria de Economía Social
                                            </li>
                                            <li>
                                                <table class="table table-striped">
                                                    <tr>
                                                        <td width="20%" style="vertical-align:middle">
                                                            <img src="{{ url('images/logo_facebook.png') }}" alt="" width="100%">
                                                        </td>
                                                        <td style="vertical-align:middle">
                                                            <a href="https://www.facebook.com/economiasocialchaco" target="_new">
                                                                <strong>EconomiaSocialChaco</strong>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align:middle">
                                                            <img src="{{ url('images/logo_twitter.png') }}" alt="" width="100%">
                                                        </td>
                                                        <td style="vertical-align:middle">
                                                            <a href="https://twitter.com/economiasocialchaco" target="_new">
                                                                <strong>@economiaSocialChaco</strong>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align:middle">
                                                            <img src="{{ url('images/logo_telefono.png') }}" alt="" width="100%">
                                                        </td>
                                                        <td style="vertical-align:middle">
                                                            <a href="tel:+5493624446996"><strong>(3624)446996</strong></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align:middle">
                                                            <img src="{{ url('images/logo_direccion.png') }}" alt="" width="100%">
                                                        </td>
                                                        <td style="vertical-align:middle">
                                                            <a href="https://www.google.com.ar/maps/place/Jos%C3%A9+Mar%C3%ADa+Paz+1620,+H3506EUM+Resistencia,+Chaco/@-27.4657009,-59.0014297,17z/data=!3m1!4b1!4m5!3m4!1s0x94450c90b554b7f1:0xf4456424e0c70f13!8m2!3d-27.4657009!4d-58.999241" target="_new"><strong>Av. Sarmiento y Marcelo T. de Alvear, Resistencia, Chaco</strong></a>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                    </div>
            </section>
          </div>
    </div>
</div>

    <script src="{{ url('js/rating.js') }}"></script>


    <script>

        (function() {

            'use strict';

                // SHOP ELEMENT
                var shop = document.querySelector('#shop');

                var data = <?php echo $json ?>;

                // INITIALIZE
                (function init() {
                    for (var i = 0; i < data.length; i++) {
                        addRatingWidget(buildShopItem(data[i]), data[i]);
                    }
                })();

                // BUILD SHOP ITEM
                function buildShopItem(data) {
                    var shopItem = document.createElement('div');

                    var html = '<div class="box" style="height: 100px;"><div class="text-box">' +
                    '<h4><a href="busquedas/oficios/'+ data.id +'">'+ data.responsable +'</a></h4>' +
                    '<strong style="color: #1b8af3;">'+ data.oficio+'</strong>' +
                    '<div class="clearfix">' +
                            '<strong><i class="fa fa-map-marker"></i>'+ data.direccion +'</strong>' +
                            '<strong><i class="fa fa-phone"></i>'+ data.telefono +'</strong>' +
                            '<strong><i class="fa fa-mail-forward"></i>'+ data.email +'</strong>' +
                    '<div class="clearfix"></br>' +
                    '<ul class="c-rating"></ul>' +
                    '</div>';

                    shopItem.classList.add('c-shop-item');
                    shopItem.innerHTML = html;
                    shop.appendChild(shopItem);

                    return shopItem;
                }

                // ADD RATING WIDGET
                function addRatingWidget(shopItem, data) {
                    var ratingElement = shopItem.querySelector('.c-rating');
                    var currentRating = data.rating;
                    var maxRating = 5;
                    var callback = function(rating) {

                    };
                    var r = rating(ratingElement, currentRating, maxRating, callback);
                }

            })();

        </script>


        @endsection