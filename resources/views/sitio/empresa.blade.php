<?php
use \App\Area;
?>
@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <!--INNER BANNER START-->
    <section id="inner-banner">
        <div class="container">
            <h1>Acerca de la Empresa</h1>
        </div>
    </section>

    <!--INNER BANNER END-->
    <!--MAIN START-->

    <div id="main">
        <!--RECENT JOB SECTION START-->
        <section class="recent-row padd-tb job-detail">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <div id="content-area">
                            <div class="box">
                                <div class="thumb">
                                    @if($empresa->logo != NULL)
                                        <a href="{{ route('empresa.show',$empresa->id) }}"><img src="{{ url('images/empresas/'.$empresa->logo) }}" alt="img"></a>
                                    @endif
                                </div>
                                <div class="text-col">
                                    <h2><a href="#">{{ $empresa->razon_social }}</a></h2>
                                    <ul class="company-small">
                                        <li><strong>Area</strong> {{ Area::find($empresa->area_id)->nombre }}</li>
                                        <li><strong>Ubicacion:</strong> Chaco, Resistencia </li>
                                    </ul>
                                    <div class="clearfix">
                                        <div class="company-social">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <h4>Sinopsis</h4>
                                    <p>{{ $empresa->sinopsis }}</p>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="col-md-3 col-sm-4">
                        <aside>
                            <div class="sidebar">
                                <h2>Recientes</h2>
                                <div class="sidebar-jobs">
                                    <ul>
                                        @foreach($ofertas as $row)
                                        <li>
                                            <a href="#">{{ $row->descripcion_corta }}</a>
                                            <span><i class="fa fa-map-marker"></i>Resistencia, Chaco </span>
                                            <span><i class="fa fa-calendar"></i>{{ $row->fecha_inicio->format('d/m/Y') }}</span>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>

        <!--RECENT JOB SECTION END-->

    </div>

    <!--MAIN END-->

@endsection
