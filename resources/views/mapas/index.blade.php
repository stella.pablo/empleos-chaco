<!DOCTYPE html>
<html>
<head>
    <title>Empleo y Trabajo - Chaco </title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/color.css') }}" rel="styleshe
et" type="text/css">
    <link href="{{ url('css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <script src="{{ url('bootstrap/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ url('bootstrap/js/bootstrap.min.js') }}"></script>


    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }

        #menuSuperior{
           position: absolute;            
           height: 10px; 
           top: 10px; 
           right: 10px;
       }

       #logoGobierno{
           position: absolute;
           bottom: 30px;           
           left: 5px;
       }

   </style>
</head>

<body>

    <div id="map"></div>

    <div id="logoGobierno">
        <img src="{{ url('images/logoMapa.png') }}" alt="">
    </div>

    <div id="menuSuperior">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Empleo en Chaco<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                      @if(auth()->user())
                          <li>
                              <a href="{{ url('postulantes/create') }}">Ver CV</a>
                          </li>
                      @else
                          <li>
                              <a href="{{ url('login') }}">Carga tu CV</a>
                          </li>
                      @endif

                      <li><a href="{{ url('guia/emprendedores') }}">Emprendedores</a></li>
                      <li><a href="{{ url('guia/oficios') }}">Oficios</a></li>
                      <li><a href="{{ url('blog') }}">Noticias</a></li>
                      <li><a href="{{ url('registro') }}">Registrar puesto</a></li>
                      <li><a href="{{ url('contacto') }}">Contáctanos</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
</div>

{{-- visualización del mapa y marcadores --}}
<script>

    var map;
    function initMap() {

         map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -27.451006, lng: -58.986620},
            zoom: 14
        });

        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setCenter(pos);
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        @foreach($avisosGps as $avisogps)

        var contenido{{ $avisogps->id }} =
        '<div id="content">'+
            '<strong>SE NECESITA</strong>'+
            '<h3 id="firstHeading" class="firstHeading">{{$avisogps->puesto}}</h3>'+
            '<div id="bodyContent">'+
                '<strong>Solicitante: </strong>{{$avisogps->nombre}}</br>'+
                '<strong>Dirección: </strong>{{$avisogps->direccion}}</br>'+
                '<p align="justify"><strong>Detalles:</strong>{{$avisogps->detalles}}</p>'+
            '</div>'+
        '</div>';

        {{--Configuraciones del marcador--}}
        var infowindow{{ $avisogps->id }} = new google.maps.InfoWindow({
            content: contenido{{ $avisogps->id }}
        });

        var marker{{ $avisogps->id }} = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: {lat: {{$avisogps->latitud}}, lng: {{$avisogps->longitud}}},
            map: map,
            title: '{{$avisogps->nombre}}',
            icon: '{{url('images/icon-mapa.png')}}'
        });

        marker{{ $avisogps->id }}.addListener('click', function() {
            infowindow{{ $avisogps->id }}.open(map, marker{{ $avisogps->id }});
        });

        @endforeach

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                    'No se detecta su ubicación.' :
                    'Error: Your browser doesn\'t support geolocation.');
        }


    }

</script>
    <script src="{{ url('js/jquery-1.11.3.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/owl.carousel.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgz4CsEGtO03T8G3RRVN3ldH5V1h1v8hY&callback=initMap"
async defer></script>
</body>
</html>