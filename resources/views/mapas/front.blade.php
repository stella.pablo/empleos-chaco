@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    <style>
        #logoGobierno{
            position: absolute;
            bottom: 30px;
            left: 5px;
        }

    </style>


    <div id="map"></div>


@endsection
@section('script')
    {{-- visualización del mapa y marcadores --}}
    <script>

        var map;
        function initMap() {

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -27.451006, lng: -58.986620},
                zoom: 12
            });

            var infoWindow = new google.maps.InfoWindow({map: map});

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    map.setCenter(pos);
            }

                    @foreach($avisosGps as $avisogps)

            var contenido{{ $avisogps->id }} =
                            '<div id="content">'+
                            '<strong>SE NECESITA</strong>'+
                            '<h3 id="firstHeading" class="firstHeading">{{$avisogps->puesto}}</h3>'+
                            '<div id="bodyContent">'+
                            '<strong>Solicitante: </strong>{{$avisogps->nombre}}</br>'+
                            '<strong>Dirección: </strong>{{$avisogps->direccion}}</br>'+
                            '<p align="justify"><strong>Detalles: </strong>{{$avisogps->detalles}}</p>'+
                            '</div>'+
                            '</div>';

                    {{--Configuraciones del marcador--}}
            var infowindow{{ $avisogps->id }} = new google.maps.InfoWindow({
                        content: contenido{{ $avisogps->id }}
                    });

            var marker{{ $avisogps->id }} = new google.maps.Marker({
                animation: google.maps.Animation.DROP,
                position: {lat: {{$avisogps->latitud}}, lng: {{$avisogps->longitud}}},
                map: map,
                title: '{{ utf8_encode($avisogps->nombre) }}',
                icon: '{{url('images/icon-mapa.png')}}'
            });

            marker{{ $avisogps->id }}.addListener('click', function() {
                infowindow{{ $avisogps->id }}.open(map, marker{{ $avisogps->id }});
            });

            @endforeach

        }}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgz4CsEGtO03T8G3RRVN3ldH5V1h1v8hY&callback=initMap"
            async defer>

    </script>
@endsection