@extends('template.admin_template')

@section('titulo')
    Mapa de Empleo - Puntos de geolocalización
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Puntos de geolocalización</a></li>
        <li class="active">Grid</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div id="bodyContenido" >
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Puesto</th>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Estado</th>
                                <th></th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('layouts.flash')
                            @foreach($puntos as $row)
                                <tr>
                                    <td>{{ $row->nombre }}</td>
                                    <td>{{ $row->direccion }}</td>
                                    <td>{{ $row->puesto }}</td>
                                    <td>{{ $row->latitud }}</td>
                                    <td>{{ $row->longitud }}</td>
                                    <td>
                                        @if($row->estado == NULL)
                                            <button class="btn btn-block btn-danger btn-xs">Desactivado</button>
                                        @else
                                            <button class="btn btn-block btn-info btn-xs">Activado</button>
                                        @endif
                                    </td>
                                    <td>{{ $row->detalles }}</td>
                                    <td>
                                        <a style="margin: 0.5px;" href="{{ route('admin.puntos.activar',$row->id) }}" title="Activar Punto" class="btn btn-default btn-sm"><i class="fa fa-fw fa-stack-exchange "></i></a>
                                        <a style="margin: 0.5px;"href="{{ route('admin.puntos.destroy',$row->id) }}" title="Eliminar Punto" onclick="return confirm('Seguro que desde deshabilitar el punto de ubicacion ?')" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
@section('recursos')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                }
            });
        });
    </script>

@endsection