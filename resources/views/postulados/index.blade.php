@extends('template.admin_template')

@section('titulo')
    Postulados a ofertas
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Postulados</a></li>
        <li class="active">Grid</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div id="bodyContenido" >
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nombre y Apellido</th>
                                <th>Oferta</th>
                                <th>Empresa</th>
                                <th>Efectuada el</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('layouts.flash')
                            @foreach($postulados as $row)
                                <tr>
                                    <td>{{ $row->nombre }}, {{ $row->apellido }}</td>
                                    <td>{{ $row->descripcion_corta  }}</td>
                                    <td>{{ \App\Empresa::find($row->empresa_id)->razon_social }}</td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>
                                        @if($row->cv != NULL)
                                            <a title="Descargar CV" target="_blank" href="{{ url('cv_postulantes/'. $row->cv) }}" class="btn btn-default btn-sm"><i class="fa fa-fw fa-download"></i></a>
                                        @endif
                                        <a href="{{ route('admin.postulantes.show',$row->usuario_id) }}" title="Ver info" class="btn btn-success btn-sm "><i class="fa fa-fw fa-check-square-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
    @endsection
    @section('recursos')
            <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                }
            });
        });
    </script>

@endsection