@extends('template.admin_template')

@section('titulo')
    Emprendedores
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Emprendedores</a></li>
        <li class="active">Editar</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-10'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencioonclick="return confirm('Seguro que desde deshabilitar la empresa ?')"n!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                @include('layouts.flash')
                {!! Form::model($emprendedor, array('method' => 'PUT', 'route' => array('admin.emprendedores.update', $emprendedor->id), 'class' => 'form-horizontal', 'files'=>'true')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Emprendimiento</label>
                        <div class="col-sm-8">
                            {!! Form::text('nombre', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Responsable</label>
                        <div class="col-sm-6">
                            {!! Form::text('responsable', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Categoria</label>
                        <div class="col-sm-6">
                            {!! Form::select('categoria_emprendedor_id', $categorias ,  null, ['class' => 'form-control col-sm-4'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">CUIT</label>
                        <div class="col-sm-4">
                            {!! Form::text('cuit', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-6">
                            {!! Form::text('direccion', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-4">
                            {!! Form::text('telefono', null, ['class' => 'form-control','placeholder'=>'0000-0000'])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descripción del puesto</label>
                        <div class="col-sm-10">
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control','id'=>'editor1'])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Logo</label>
                        <div class="col-sm-6">
                            {!! Form::file('logo') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                           <img style="width: 30%;" src="{{ url('images/emprendedores/'. $emprendedor->logo) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Galeria</label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control" name="galeria[]" multiple>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            @foreach($galeria as $row)
                                <a href="{{ route('admin.galeria.delete',[$row->id,$emprendedor->id]) }}" class="confirmation" >
                                    <img class="img-responsive hvr-float" style="margin: 6px; width: 200px ; height:150px" onclick="return confirm('Seguro que desea eliminar la imagen ?')" src="{{ url('images/galeria/'. $row->nombre) }}" alt="">
                                </a>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            {!! Form::text('email', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancelar</button>
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->

@endsection
@section('recursos')
            <script type="javascript">

                $('.confirmation').click(function (e) {
                    var href = $(this).attr('href');

                    swal({
                                title: "Are you sure?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                cancelButtonText: "No, cancel plx!",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    window.location.href = href;
                                }
                            });

                    return false;
                });

            </script>
@endsection



