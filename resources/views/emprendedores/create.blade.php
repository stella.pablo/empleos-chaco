@extends('template.admin_template')

@section('titulo')
    Emprendedores
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Emprendedores</a></li>
        <li class="active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('route' => 'admin.emprendedores.store', 'class' => 'form-horizontal','files'=>'true')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Emprendimiento</label>
                        <div class="col-sm-8">
                            {!! Form::text('nombre', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Responsable</label>
                        <div class="col-sm-6">
                            {!! Form::text('responsable', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Categoria</label>
                        <div class="col-sm-6">
                            {!! Form::select('categoria_emprendedor_id', $categorias ,  null, ['class' => 'form-control col-sm-4'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">CUIT</label>
                        <div class="col-sm-4">
                            {!! Form::text('cuit', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-6">
                            {!! Form::text('direccion', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-4">
                            {!! Form::text('telefono', null, ['class' => 'form-control','placeholder'=>'0000-0000'])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descripción</label>
                        <div class="col-sm-10">
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control','id'=>'editor1'])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Logo</label>
                        <div class="col-sm-6">
                            {!! Form::file('logo') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Galeria</label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control" name="galeria[]" multiple>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            {!! Form::text('email', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancelar</button>
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>


                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
@endsection
