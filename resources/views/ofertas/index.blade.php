<?php
use App\Empresa;
use App\Area;
?>
@extends('template.admin_template')

@section('titulo')
    Ofertas
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Ofertas</a></li>
        <li class="active">Grid</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-2">
                <a href="{{ route('admin.ofertas.create') }}" class="btn btn-block btn-success">Nuevo</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div id="bodyContenido" >
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Empresa</th>
                                <th>Area</th>
                                <th>Puestos</th>
                                <th>Fecha Inicio</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('layouts.flash')
                            @foreach($ofertas as $row)
                                @if($row->destacar == 1)
                                <tr style="background-color: #f7bc60; ">
                                @else
                                    <tr>
                                @endif
                                    <td>{{ $row->descripcion_corta }}</td>
                                    <td>{{ Empresa::find($row->empresa_id)->razon_social }}</td>
                                    <td>{{ Area::find($row->area_id)->nombre }}</td>
                                    <td>{{ $row->puestos }}</td>
                                    <td>
                                        @if($row->fecha_inicio != NULL)
                                            {{ $row->fecha_inicio->format('d/m/Y') }}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.ofertas.edit',$row->id) }}"  style="margin: 1px" class="btn btn-warning btn-sm "><i class="fa fa-fw fa-edit"></i></a>
                                        @if($row->deleted_at == NULL)
                                            @if($row->destacar == 0)
                                                <a href="{{ route('admin.ofertas.destacar',$row->id) }}" title="Destacar" style="margin: 1px" class="btn btn-info btn-sm"><i class="fa fa-fw fa-arrow-circle-up"></i></a>
                                            @else
                                                <a href="{{ route('admin.ofertas.nodestacar',$row->id) }}" title="No Destacar" style="margin: 1px" class="btn btn-success btn-sm"><i class="fa fa-fw fa-arrow-circle-down"></i></a>
                                            @endif
                                            <a href="{{ route('admin.ofertas.destroy',$row->id) }}" style="margin: 1px;" onclick="return confirm('Seguro que desde deshabilitar la oferta ?')" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-remove"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
    @endsection
    @section('recursos')
            <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                }
            });
        });
    </script>

@endsection