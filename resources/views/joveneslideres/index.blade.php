@extends('joveneslideres.master')
@section('content')


<div class="main-container">
    <section class="hero-slider">
        <ul class="slides">
            <li class="overlay">
                <div class="background-image-holder parallax-background">
                    <img class="background-image" alt="Background Image" src="{{ url('lideres/img/portada0.jpg') }}">
                </div>

                <div class="container align-vertical">
                    <div class="row">
                        <div class="col-md-6 col-sm-9">
                            <h1 class="text-white">CREDITO JOVEN, información necesaria para calificar en el otorgamiento del mismo</h1>
                            <!--<a target="_blank" href="" class="btn btn-primary btn-white">Customize</a>-->
                            <a href="{{ url('joveneslideres/financiamiento') }}" class="btn btn-primary btn-filled">Ver Financiamiento</a>
                        </div>
                    </div>
                </div><!--end of container-->
            </li><!--end of individual slide-->

            <!--end of individual slide-->

            <li class="overlay">
                <div class="background-image-holder parallax-background">
                    <img class="background-image" alt="Background Image" src="{{ url('lideres/img/portada2.jpg') }}">
                </div>

                <div class="container align-vertical">
                    <div class="row">
                        <div class="col-md-6 col-sm-9">
                            <h1 class="text-white">Si tenés alguna idea en desarrollo o a desarrollar,  envianos tu proyecto para evaluarlo.</h1>
                            <!--<a target="_blank" href="" class="btn btn-primary btn-white">Customize</a>-->
                            <a href="{{ url('joveneslideres/inscripcion') }}" class="btn btn-primary btn-filled">Inscripción</a>
                        </div>
                    </div>
                </div><!--end of container-->
            </li><!--end of individual slide-->

            <li class="overlay">
                <div class="background-image-holder parallax-background">
                    <img class="background-image" alt="Background Image" src="{{ url('lideres/img/portada1.jpg') }}">
                </div>

                <div class="container align-vertical">
                    <div class="row">
                        <div class="col-md-6 col-sm-9">
                            <h1 class="text-white">Toda Nuestra Programación, en programas, cursos y liderazgo. </h1>
                            <!--<a target="_blank" href="" class="btn btn-primary btn-white">Customize</a>-->
                            <a href="{{ url('joveneslideres/agenda') }}" class="btn btn-primary btn-filled">Ver Agenda</a>
                        </div>
                    </div>
                </div><!--end of container-->
            </li><!--end of individual slide-->


        </ul>
    </section>

    <section class="primary-features duplicatable-content">
        <div class="container">
            <div class="row">

                <div class="col-md-4 col-sm-6 clearfix">
                    <div class="feature feature-icon-small">
                        <i class="icon icon-target"></i>
                        <h6 class="text-white">Objetivo</h6>
                        <p class="text-white">
                            El programa Jóvenes Líderes tiene como objetivo fomentar y generar el espíritu  de liderazgo en los jóvenes, potenciando sus capacidades humanas y profesionales.
                        </p>
                    </div><!--end of feature-->
                </div>

                <div class="col-md-4 col-sm-6 clearfix">
                    <div class="feature feature-icon-small">
                        <i class="icon icon-document"></i>
                        <h6 class="text-white">Destinatarios</h6>
                        <p class="text-white">
                            Jóvenes  de  18 a 30 años de la provincia del Chaco que desean emprender un nuevo proyecto o potenciar su emprendimiento ya existente.
                        </p>
                    </div><!--end of feature-->
                </div>

                <div class="col-md-4 col-sm-6 clearfix">
                    <div class="feature feature-icon-small">
                        <i class="icon icon-linegraph"></i>
                        <h6 class="text-white">Crédito Jóven</h6>
                        <p class="text-white">
                            El joven podrá acceder a un crédito desde $50.000 hasta $100.000, una vez que cumpla con cada una de las etapas del “Programa Ideas Acción”.
                        </p>
                    </div><!--end of feature-->
                </div>

            </div><!--end of row-->

        </div><!--end of container-->
    </section>

    <section class="side-image text-heavy clearfix">
        <div class="image-container col-md-5 col-sm-3 pull-left">
            <div class="background-image-holder">
                <img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen1.jpg') }}">
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-4 content clearfix">
                    <h1>Jóvenes Lideres Chaco, propone</h1>
                    <p class="lead">
                        Incentivar el espíritu emprendedor y de liderazgo a través de un programa destinado a jóvenes de entre 17 y 35 años que se encuentren iniciando sus primeros pasos en el mercado laboral y/o comercial.
                    </p>
                    <p class="lead">
                        Brindar herramientas técnicas para el armado de proyectos orientados al otorgamiento de microcréditos de hasta 50 mil pesos a través del Fondo Fiduciario para la Promoción del Empleo Joven.
                    </p>
                    <a href="contactenos.blade.php" class="btn btn-primary">Contáctenos</a><br>
                    <!--<div class="col-sm-6 no-pad-left feature">
                        <h5>Festival de Emprendedores</h5>
                        <p>
                            Puestos, stands y food trucks de emprendedores de la provincia.
                        </p>
                        <p>
                            Músicos invitados: Soledad Pastorutti  o Luciano Pereyra o  Abel Pintos
                        </p>
                    </div>
                    <div class="col-sm-6 no-pad-left feature">
                        <h5>“Historias que inspiran” </h5>
                        <p>
                            <li><strong>Alejandro Tamer</strong> - CEO de Despegar Argentina.</li>
                            <li><strong>Diego Noriega</strong> - Fundador de Alamaula.</li>
                            <li><strong>José Como Birche</strong>, Jefe del servicio de Cirugía Cardiovascular.</li>

                        </p>
                    </div>-->
                </div>

            </div><!--end of row-->

        </div>
    </section>

    <!--<section class="duplicatable-content">

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Programas, Cursos y Liderazgo</h1>
                </div>
            </div><!--end of row

            <div class="row">
                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-pencil"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Programa POETA Jóvenes</h5>
                            <p>
                                Es un modelo de desarrollo social que busca consolidar y fortalecer una red de Centros Tecnológicos dirigidos por...</br><a target="_blank" href="formacion.blade.php#1"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-linegraph"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Programa Emprendedores</h5>
                            <p>
                                Programa intensivo de 4 encuentros al mes, en el que un profesional brinda herramientas para traducir las ideas de...</br><a target="_blank" href="formacion.blade.php#2"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-tools-2"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Desarrollo de APP para emprendedores</h5>
                            <p>
                                Aplicación de descarga gratuita con contenido para aquellos emprendedores de... </br><a target="_blank" href="herramientas.blade.php#7"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-global"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Convenios Internacionales</h5>
                            <p>
                                Paquete de convenios internacionales para la participación de jóvenes de la provincia en eventos de forma...</br><a target="_blank" href="formacion.blade.php#3"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-genius"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Foro Regional de Emprendedores</h5>
                            <p>
                                Realización de un foro regional de emprendedores, donde los actores principales puedan debatir respecto a las necesidades del...</br><a target="_blank" href="formacion.blade.php#4"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-tablet"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Conversatorio On-line</h5>
                            <p>
                                Se hará vía streaming y mediante la App. Entrevistas vía streaming con diversos referentes de...</br><a target="_blank" href="formacion.blade.php#5"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-calendar"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Ciclo de Conferencias Presenciales</h5>
                            <p>
                                Con entrada libre y gratuita y a desarrollarse principalmente en Universidades y centros de formación académica, se presentarán...</br><a target="_blank" href="formacion.blade.php#6"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-desktop"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Desarrollo de Web </h5>
                            <p>
                                Conectar a freelancers de Chaco con posibles clientes del resto del país, la Región y el mundo para potenciar sus...</br><a target="_blank" href="herramientas.blade.php#8"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

                <!--<div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-presentation"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Programa Junior Achievement (JA)</h5>
                            <p>
                                En Argentina cuenta con sedes en Buenos Aires, Córdoba, Mendoza, Santa Fe, Salta, Tucumán y Jujuy...</br><a target="_blank" href="#"><strong>Leer más</strong></a>
                            </p>
                        </div>
                    </div>
                </div><!--end 6 col

            </div><!--end of row
        </div>

    </section>-->

    <section class="side-image clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-6 content col-sm-8 clearfix">
                    <h1><a href="agenda.blade.php">Agenda y Programación</a></h1>

                    <ul class="blog-snippet-2">
                        <li>
                            <div class="icon">
                                <i class="icon icon-pencil"></i>
                            </div>
                            <div class="title">
                                <a target="_blank" href="../../../public/lideres/publicaciones_agenda/ejemplofoto.html">Agenda 1 - Ejemplo Foto</a>
                                <span class="sub alt-font">Publicado el 1 De Mayo 2017</span>
                            </div>
                        </li>

                        <li>
                            <div class="icon">
                                <i class="icon icon-calendar"></i>
                            </div>
                            <div class="title">
                                <a target="_blank" href="../../../public/lideres/publicaciones_agenda/ejemplovideo.html">Agenda 2 - Ejemplo Video</a>
                                <span class="sub alt-font">Publicado el 1 De Mayo 2017</span>
                            </div>
                        </li>

                        <li>
                            <div class="icon">
                                <i class="icon icon-newspaper"></i>
                            </div>
                            <div class="title">
                                <a target="_blank" href="../../../public/lideres/publicaciones_agenda/ejemplofoto.html">Agenda 3 - Ejemplo Foto</a>
                                <span class="sub alt-font">Publicado el 1 De Mayo 2017</span>
                            </div>
                        </li>

                        <li>
                            <div class="icon">
                                <i class="icon icon-pencil"></i>
                            </div>
                            <div class="title">
                                <a target="_blank" href="../../../public/lideres/publicaciones_agenda/ejemplovideo.html">Agenda 4 - Ejemplo Video</a>
                                <span class="sub alt-font">Publicado el 1 De Mayo 2017</span>
                            </div>
                        </li>
                    </ul>

                </div><!--end of row-->


            </div><!--end of container-->
        </div>

        <div class="image-container col-md-5 col-sm-3 pull-right">
            <div class="background-image-holder">
                <img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen2.jpg') }}">
            </div>
        </div>

    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <i class="icon icon-jumbo social_twitter"></i>
                    <i></i>
                    <i class="icon icon-jumbo social_facebook"></i>
                    <p>
                        Síguenos en Twitter <strong><a target="_blank" href="https://twitter.com/SecEmpleoChaco">@SecEmpleoChaco</a></strong> y en nuestra <strong><a target="_blank" href="https://www.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547">Fan Page</a></strong> de Facebook para estar siempre actualizado!
                    </p>
                </div>
            </div><!--end of row-->
        </div><!--end of container-->
    </section>

    <section class="no-pad clearfix">

        <div class="col-md-6 col-sm-12 no-pad">

            <div class="feature-box">

                <div class="background-image-holder overlay">
                    <img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen7.jpg') }}">
                </div>

                <div class="inner">
                    <span class="alt-font text-white">Financiamiento</span>
                    <h1 class="text-white">REQUISITOS </br>CREDITO JOVEN </h1>
                    <p class="text-white">
                        Información necesaria para  "CREDITO JOVEN", junto a los requisitos previos para calificar y otorgamiento del mismo...
                    </p>
                    <a href="financiamiento.blade.php" class="btn btn-primary btn-white">Leer Más</a>
                </div>
            </div>

        </div>

        <div class="col-md-6 col-sm-12 no-pad">

            <div class="feature-box">

                <div class="background-image-holder overlay">
                    <img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen6.jpg') }}">
                </div>

                <div class="inner">
                    <span class="alt-font text-white">Inscripción</span>
                    <h1 class="text-white">CONTANOS </br>TU PROYECTO</h1>
                    <p class="text-white">
                        Si tenés alguna idea en desarrollo o a desarrollar,  envianos tu proyecto para evaluarlo.
                    </p>
                    <a href="inscripcion.blade.php" class="btn btn-primary btn-white">Leer Más</a>
                </div>
            </div>
        </div>

    </section>

    <section class="clients-2">
        <div class="container">
            <div class="row">

                <div class="col-md-4 col-sm-4">
                    <img alt="" src="{{ url('lideres/img/sponsor2.png') }}">
                </div>

                <div class="col-md-4 col-sm-4">
                    <img alt="" src="{{ url('lideres/img/sponsor1.png') }}">
                </div>

                <div class="col-md-4 col-sm-4">
                    <img alt="" src="{{ url('lideres/img/sponsor3.png') }}">
                </div>


            </div><!--end of row-->
        </div><!--end of container-->
    </section>

</div>

@endsection