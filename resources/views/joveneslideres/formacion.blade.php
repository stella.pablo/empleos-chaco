<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    
<head>
        <meta charset="utf-8">
        <title>Jóvenes Líderes Chaco | Formación</title>
        <meta name="description" content="El programa Jóvenes Líderes tiene como objetivo fomentar y generar el espíritu  de liderazgo en los jóvenes, potenciando sus capacidades humanas y profesionales.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="FEELWIT">
		<link rel="shortcut icon" type="image/png" href="../../../public/lideres/img/favicon.png"/>
		<link href="../../../public/lideres/css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/theme-aquatica.css" rel="stylesheet" type="text/css" media="all"/>
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../../../public/lideres/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
    	<div class="loader">
    		<div class="spinner">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>
    	</div>
				
		<div class="nav-container">
			<nav class="top-bar">
				<div class="container">
				
					<div class="row nav-menu">
						<div class="col-sm-2 col-md-2 columns">
							<a href="index.blade.php">
								<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo1.png">
							</a>
							
							<a href="index.blade.php">
								<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo2.png">
							</a>
							
						</div>
					
						<div class="col-sm-8 col-md-8 columns">
							<ul class="menu">
								<li><a href="index.blade.php">Inicio</a>
								<li><a href="financiamiento.blade.php">Financiamiento</a>
								<!--<li><a href="formacion.blade.php">Formación</a>-->
								<!--<li><a href="herramientas.blade.php">Herramientas</a>-->
								<li><a href="agenda.blade.php">Agenda</a>
								<li><a href="inscripcion.blade.php">Inscripción</a>
								<li><a href="contactenos.blade.php">Contacto</a>
								
							</ul>
						
						</div>
						
						<div class="col-sm-2 col-md-2 columns">
							<a href="index.blade.php">
									<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo.png">
								</a>
						</div>
					</div><!--end of row-->
					
					<div class="mobile-toggle">
						<i class="icon icon_menu"></i>
					</div>
					
				</div><!--end of container-->
			</nav>
		</div>
		<div class="main-container">
			<header class="signup"> 
					<div class="background-image-holder parallax-background">
						<img class="background-image" alt="Background Image" src="../../../public/lideres/img/imagen5.jpg">
					</div>
					
					<div class="container">
						<div class="row">
							<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
								<h1 class="text-white">Todos nuestros Cursos y Programas de formación</h1>
							</div>
						</div><!--end of row-->
						
					</div><!--end of container-->	
			</header>
			
			<section class="accordion-section" id="1">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-grey alt-font">Programa</span>
							<h1 class="text-grey">POETA Jóvenes</h1>
						</div>
					</div><!--end of row-->
					
						<div class="col-sm-5 col-md-4 align-vertical no-align-mobile">
							<ul class="icons-large">
								
								<li><i class="icon icon-book-open"></i></li>
								<li><i class="icon icon-pencil"></i></li>
							</ul>
							<h1 class="no-margin-bottom">CONVENIO CON ORGANIZACIÓN DE ESTADOS AMERICANOS (TRUST-OEA)</h1>
						</div>
					
						<div class="col-sm-7 col-md-4">
							<ul class="accordion">
								<li class="active">
									<div class="title"><span>En que consiste?</span></div>
									<div class="text">
										<p align="justify">
											Es un modelo de desarrollo social que busca consolidar y fortalecer una red de Centros Tecnológicos dirigidos por ONGs, entidades del sector público, privado e instituciones académicas, a través de una oferta metodológica compuesta de recursos, servicios y beneficios para la promoción de la inclusión social y la generación de oportunidades económicas y educativas en poblaciones vulnerables.
										</p>
										<p align="justify">
											Se implementa a través de una Red de Centros Tecnológicos (Centros POETA Jóvenes). 
										</p>
									</div>
								</li>
							
								<li>
									<div class="title"><span>Cursos principales</span></div>
									<div class="text">
										<p>
											- Alfabetización Digital (20 horas).</br>- Preparación para el Mundo del Trabajo.</br>- Como buscar empleo.</br>- Educación Ciudadana y Valores.
										</p>
										
									</div>
								</li>
							
								<li>
									<div class="title"><span>Cursos complementarios</span></div>
									<div class="text">
										<p>
											- Educación Económica y Financiera.</br>- La idea de  negocio.</br>- Emprende.
										</p>
										
									</div>
								</li>
							
								<li>
									<div class="title"><span>Productos de Asesoría</span></div>
									<div class="text">
										<p>
											- Guía de Empresarismo Social para Centros POETA.</br>- Guía metodológica para el facilitador y cuaderno práctico del alumno.

										</p>
										
									</div>
								</li>
							</ul><!--end of accordion-->
						</div>
					
						<div class="col-md-4 hidden-sm align-vertical no-align-mobile">
							<!--<div class="feature">
								<h5>En Argentina</h5>
								<p>
									Existe un solo centro “AES POETA Jóvenes” ubicado en San Nicolás, Bs. As. </br>Es operado desde 2013 por <strong>AES</strong> Argentina y la <strong>Municipalidad de San Nicolás</strong>.
								</p>
							</div>-->
						
							<!--<div class="feature">
								<h5>Franquicia Social POETA Jóvenes en Chaco</h5>
								<p>
									Se propone desarrollar, consolidar y fortalecer una red de Centros Tecnológicos en Chaco, que promuevan oportunidades económicas, educativas y de inclusión social a través del programa POETA Jóvenes.

								</p>
							</div>-->
						</div>
					</div><!--end of row-->
				</div>
			</section>
			
			<section class="primary-features duplicatable-content" id="2">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-white alt-font">Programa </span>
							<h1 class="text-white">Emprendedores</h1>
						</div>
					</div><!--end of row-->
					<div class="row">
						<div class="text-white col-md-12 text-center">
							<h3 class="text-white">Ideas en Acción</h3>
							<p>
								Programa intensivo de 4 encuentros al mes, en el que un profesional brinda herramientas para traducir las ideas de los emprendedores en algo claro y concreto. Durante el proceso cada uno de los participantes irá identificando lo necesario para llevar sus objetivos a la acción.</br>Se busca que las personas descubran sus habilidades, capacidades y deseos, y reconozcan sus limitaciones trabajando en el logro de objetivos mediante técnicas de PNL y coaching. 

							</p>
						</div>
					</div><!--end of row-->
					</br>
					</br>
					<div class="row">
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									
									<h6 class="text-white">El Programa Incluye:</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked icon-large"></i>
									<h6 class="text-white">Mentor personal</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked icon-large"></i>
									<h6 class="text-white">Coaching</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked icon-large"></i>
									<h6 class="text-white">4 talleres intensivos</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked"></i>
									<h6 class="text-white">Asesor experto para despejar todas las dudas</h6>
									
								</div><!--end of feature-->
							</div>
						
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked"></i>
									<h6 class="text-white">Guías prácticas</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked icon-large"></i>
									<h6 class="text-white">Acompañamiento para armar un plan de acción</h6>
									
								</div><!--end of feature-->
							</div>
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked"></i>
									<h6 class="text-white">Programas de Financiamiento: previa aprobación</h6>
									
								</div><!--end of feature-->
							</div>
						
							<div class="col-md-4 col-sm-6 clearfix">
								<div class="feature feature-icon-small">
									<i class="icon icon_box-checked"></i>
									<h6 class="text-white">Acompañamiento para la presentación final ante un comité asesor</h6>
									
								</div><!--end of feature-->
							</div>
							
		
					</div><!--end of row-->
				
				</div><!--end of container-->
			</section>
			
			<section class="product-right" id="3">
			
				<div class="background-image-holder">
					<img class="background-image" alt="Background Image" src="../../../public/lideres/img/grey-bg.jpg">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-grey alt-font">Convenios</span>
							<h1 class="text-grey">Internacionales</h1>
						</div>
					</div><!--end of row-->
					
					
						<div class="col-md-5 col-sm-6">
							<h1>Paquete de convenios internacionales para la participación de jóvenes de la provincia de Chaco en eventos de forma gratuita y acceso a herramientas de otras universidades internacionales.</h1>
							<!--<p class="lead">
								Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
							</p>
							<a href="#" class="btn btn-primary">Try A Demo</a>
							<a href="#" class="btn btn-primary btn-filled">Purchase</a>-->
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
				
				<div class="product-image" data-scroll-reveal="enter right and move 100px">
					<img alt="Product Image" src="../../../public/lideres/img/conveniosinternacionales.png">
				</div>
			
			</section>
			
			<section class="product-right" id="4">
			
				<div class="background-image-holder">
					<img class="background-image" alt="Background Image" src="../../../public/lideres/img/grey-bg.jpg">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-grey alt-font">Foro</span>
							<h1 class="text-grey">Regional de Emprendedores</h1>
						</div>
					</div><!--end of row-->
					
					
						<div class="col-md-5 col-sm-6">
							<h2>ACONTECIMIENTO PLURICULTURAL REGIONAL </br>Realización de un foro regional de emprendedores, donde los actores principales puedan debatir respecto a las <strong>necesidades del sector</strong> y estrategias en el desarrollo de herramientas para el <strong>crecimiento y la potenciación con el sector público.</strong></h2>
							<!--<p class="lead">
								Realización de un foro regional de emprendedores, donde los actores principales puedan debatir respecto a las <strong>necesidades del sector</strong> y estrategias en el desarrollo de herramientas para el <strong>crecimiento y la potenciación con el sector público.</strong>
							</p>
							<a href="#" class="btn btn-primary">Try A Demo</a>
							<a href="#" class="btn btn-primary btn-filled">Purchase</a>-->
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
				
				<div class="product-image" data-scroll-reveal="enter right and move 100px">
					<img alt="Product Image" src="../../../public/lideres/img/fororegional.png">
				</div>
			
			</section>
			
			<section class="product-right" id="5">
			
				<div class="background-image-holder">
					<img class="background-image" alt="Background Image" src="../../../public/lideres/img/grey-bg.jpg">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-grey alt-font">Conversatorio</span>
							<h1 class="text-grey">On-Line</h1>
						</div>
					</div><!--end of row-->
					
					
						<div class="col-md-5 col-sm-6">
							<h1>CON REFERENTES EMPRENDEDORES DE DIFERENTES PAÍSES </br>Se hará vía <strong>streaming</strong> y mediante la <strong>App</strong>. Entrevistas vía streaming con diversos referentes de la cultura emprendedora. </h1>
							<!--<p class="lead">
								Se hará vía <strong>streaming</strong> y mediante  la <strong>App</strong>. Entrevistas vía streaming con diversos referentes de la cultura emprendedora.
							</p>
							<a href="#" class="btn btn-primary">Try A Demo</a>
							<a href="#" class="btn btn-primary btn-filled">Purchase</a>-->
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
				
				<div class="product-image" data-scroll-reveal="enter right and move 100px">
					<img alt="Product Image" src="../../../public/lideres/img/conferenciaonline.png">
				</div>
			
			</section>
			
			<section class="product-right" id="6">
			
				<div class="background-image-holder">
					<img class="background-image" alt="Background Image" src="../../../public/lideres/img/grey-bg.jpg">
				</div>
				
				<div class="container align-vertical">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-grey alt-font">Ciclo</span>
							<h1 class="text-grey">Conferencias Presenciales</h1>
						</div>
					</div><!--end of row-->
					
					
						<div class="col-md-5 col-sm-6">
							<h2>Con entrada libre y gratuita y a desarrollarse principalmente en Universidades y centros de formación académica, se presentarán diversos conferencistas de relevancia internacional quienes podrán aportar a los jóvenes participantes sus visiones respecto al <strong>progreso, al liderazgo y al emprendedurismo.</strong></h2>
							<!--<p class="lead">
								Se hará vía <strong>streaming</strong> y mediante la <strong>App</strong>. Entrevistas vía streaming con diversos referentes de la cultura emprendedora.
							</p>
							<a href="#" class="btn btn-primary">Try A Demo</a>
							<a href="#" class="btn btn-primary btn-filled">Purchase</a>-->
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
				
				<div class="product-image" data-scroll-reveal="enter right and move 100px">
					<img alt="Product Image" src="../../../public/lideres/img/cicloconferencias.png">
				</div>
			
			</section>
			
			<!--<section class="image-divider overlay duplicatable-content">
				<div class="background-image-holder parallax-background">
					<img class="background-image" alt="Background Image" src="img/hero13.jpg">
				</div>
				
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 text-center">
							<span class="text-white alt-font">Check this out</span>
							<h1 class="text-white">Our Process</h1>
						</div>
					</div><!--end of row
					
					<div class="row">
						<div class="col-sm-4">
							<div class="feature feature-icon-large">
								<i class="text-white icon icon-notebook"></i>
								<h5 class="text-white">Research</h5>
								<p class="text-white">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="feature feature-icon-large">
								<i class="text-white icon icon-genius"></i>
								<h5 class="text-white">Develop</h5>
								<p class="text-white">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="feature feature-icon-large">
								<i class="text-white icon icon-gift"></i>
								<h5 class="text-white">Deliver</h5>
								<p class="text-white">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
							</div>
						</div>
					</div><!--end of row
				</div><!--end of container
			</section>-->
			
			<section class="clients-2">
				<div class="container">
					<div class="row">
												
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor2.png">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor1.png">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor3.png">
						</div>
						
						
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
		
		<div class="footer-container">
			
		
			<footer class="short bg-secondary-1">
				<div class="container">
					<div class="row">
						<div class="col-sm-10">
							<span class="sub">© 2017 JOVENES LIDERES CHACO.</span>
							
						</div>
						
						<div class="col-sm-2 text-right">
							<ul class="social-icons">
								<li>
									<a target="_blank" href="https://twitter.com/SecEmpleoChaco">
										<i class="icon social_twitter"></i>
									</a>
								</li>
								
								<li>
									<a target="_blank" href="https://web.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547">
										<i class="icon social_facebook"></i>
									</a>
								</li>
							</ul>	
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</footer>
		</div>
				
		<script src="../../../public/lideres/js/jquery.min.js"></script>
        <script src="../../../public/lideres/js/jquery.plugin.min.js"></script>
        <script src="../../../public/lideres/js/bootstrap.min.js"></script>
        <script src="../../../public/lideres/js/jquery.flexslider-min.js"></script>
        <script src="../../../public/lideres/js/smooth-scroll.min.js"></script>
        <script src="../../../public/lideres/js/skrollr.min.js"></script>
        <script src="../../../public/lideres/js/spectragram.min.js"></script>
        <script src="../../../public/lideres/js/scrollReveal.min.js"></script>
        <script src="../../../public/lideres/js/isotope.min.js"></script>
        <script src="../../../public/lideres/js/twitterFetcher_v10_min.js"></script>
        <script src="../../../public/lideres/js/lightbox.min.js"></script>
        <script src="../../../public/lideres/js/jquery.countdown.min.js"></script>
        <script src="../../../public/lideres/js/scripts.js"></script>
    </body>

</html>
				