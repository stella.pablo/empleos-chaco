<!DOCTYPE html>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<html class="no-js lt-ie9 lt-ie8">
<html class="no-js lt-ie9">
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>Jóvenes Líderes Chaco | Emprendimiento y Calidad Laboral</title>
    <meta name="description" content="El programa Jóvenes Líderes tiene como objetivo fomentar y generar el espíritu  de liderazgo en los jóvenes, potenciando sus capacidades humanas y profesionales.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="FEELWIT">
    <link rel="shortcut icon" type="image/png" href="{{ url('lideres/img/favicon.png') }}"/>
    <link href="{{ url('lideres/css/flexslider.min.css')}}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('lideres/css/line-icons.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('lideres/css/elegant-icons.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('lideres/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('lideres/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('lideres/css/theme-aquatica.css') }}" rel="stylesheet" type="text/css" media="all"/>
    <!--[if gte IE 9]>
    <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
    <script src="{{ url('lideres/js/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
</head>
<body>
<div class="loader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
    <div class="nav-container">
    <nav class="top-bar">
        <div class="container">

            <div class="row nav-menu">
                <div class="col-sm-2 col-md-2 columns">
                    <a href="{{ url('joveneslideres') }}">
                        <img class="logo logo-dark" alt="Logo" src="{{ url('lideres/img/logo1.png') }}">
                    </a>

                    <a href="{{ url('joveneslideres') }}">
                        <img class="logo logo-dark" alt="Logo" src="{{ url('lideres/img/logo2.png') }}">
                    </a>

                </div>

                <div class="col-sm-8 col-md-8 columns">
                    <ul class="menu">
                        <li><a href="{{ url('joveneslideres') }}">Inicio</a>
                        <li><a href="{{ url('joveneslideres/financiamiento') }}">Financiamiento</a>
                        <li><a href="{{ url('joveneslideres/agenda') }}">Agenda</a>
                        <li><a href="{{ url('joveneslideres/incripcion') }}">Inscripción</a>
                        <li><a href="{{ url('joveneslideres/contacto') }}">Contacto</a>

                    </ul>

                </div>

                <div class="col-sm-2 col-md-2 columns">
                    <a href="index.html">
                        <img class="logo logo-dark" alt="Logo" src="{{ url('lideres/img/logo.png') }}">
                    </a>
                </div>
            </div><!--end of row-->

            <div class="mobile-toggle">
                <i class="icon icon_menu"></i>
            </div>

        </div><!--end of container-->
    </nav>
</div>
    @yield('content')
<div class="footer-container">
    <footer class="short bg-secondary-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-10">
                    <span class="sub">© 2017 JOVENES LIDERES CHACO.</span>

                </div>

                <div class="col-sm-2 text-right">
                    <ul class="social-icons">
                        <li>
                            <a target="_blank" href="https://twitter.com/SecEmpleoChaco">
                                <i class="icon social_twitter"></i>
                            </a>
                        </li>

                        <li>
                            <a target="_blank" href="https://web.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547">
                                <i class="icon social_facebook"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--end of row-->
        </div><!--end of container-->
    </footer>
</div>

<script src="{{ url('lideres/js/jquery.min.js') }}"></script>
<script src="{{ url('jquery.plugin.min.js') }}"></script>
<script src="{{ url('lideres/js/bootstrap.min.js') }}"></script>
<script src="{{ url('lideres/js/jquery.flexslider-min.js') }}"></script>
<script src="{{ url('lideres/js/smooth-scroll.min.js') }}"></script>
<script src="{{ url('lideres/js/skrollr.min.js') }}"></script>
<script src="{{ url('lideres/js/spectragram.min.js') }}"></script>
<script src="{{ url('lideres/js/scrollReveal.min.js') }}"></script>
<script src="{{ url('lideres/js/isotope.min.js') }}"></script>
<script src="{{ url('lideres/js/twitterFetcher_v10_min.js') }}"></script>
<script src="{{ url('lideres/js/lightbox.min.js') }}"></script>
<script src="{{ url('lideres/js/jquery.countdown.min.js') }}"></script>
<script src="{{ url('lideres/js/scripts.js') }}"></script>
</body>

</html>
