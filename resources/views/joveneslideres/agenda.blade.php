@extends('joveneslideres.master')
@section('content')

	<div class="main-container">
		<header class="page-header">
			<div class="background-image-holder parallax-background">
				<img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen8.jpg') }}">
			</div>

			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span class="text-white alt-font">AGENDA</span>
						<h1 class="text-white">Novedades y Noticias </h1>
						<p class="text-white lead">Toda la información actualizada<br>de nuestros cursos, agenda, <br>y noticias de interés.</p>
					</div>
				</div><!--end of row-->
			</div><!--end of container-->
		</header>

		<section class="blog-list-3 bg-white">

			<div class="blog-snippet-3">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<h1><a target="_blank" href="">Título Ejemplo con foto de <em>Agenda 1</em></a></h1>
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at luctus erat. Aenean molestie, odio eu mattis feugiat, urna purus tincidunt augue, eu iaculis sapien eros eget lacus. Sed dapibus leo nec urna iaculis dignissim. Nullam iaculis in risus sit amet facilisis. Nunc convallis euismod dui sit amet sodales.
							</p>
							<i class="icon icon_calendar"></i><span class="alt-font">Publicado el 1 De Mayo 2017</span><br>
							<i class="icon icon_clock_alt"></i><span class="alt-font">Lectura de 5 Minutos</span>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</div><!--end of blog-snippet-3-->

			<div class="blog-snippet-3">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<h1><a target="_blank" href="">Título Ejemplo con video de <em>Agenda 2</em></a></h1>
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at luctus erat. Aenean molestie, odio eu mattis feugiat, urna purus tincidunt augue, eu iaculis sapien eros eget lacus. Sed dapibus leo nec urna iaculis dignissim. Nullam iaculis in risus sit amet facilisis. Nunc convallis euismod dui sit amet sodales.
							</p>
							<i class="icon icon_calendar"></i><span class="alt-font">Publicado el 1 De Mayo 2017</span><br>
							<i class="icon icon_clock_alt"></i><span class="alt-font">Lectura de 4 Minutos</span>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</div><!--end of blog-snippet-3-->

		</section>

		<section class="blog-list-3 bg-white">

			<div class="blog-snippet-3">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<h1><a target="_blank" href="">Título Ejemplo con foto de <em>Agenda 3</em></a></h1>
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at luctus erat. Aenean molestie, odio eu mattis feugiat, urna purus tincidunt augue, eu iaculis sapien eros eget lacus. Sed dapibus leo nec urna iaculis dignissim. Nullam iaculis in risus sit amet facilisis. Nunc convallis euismod dui sit amet sodales.
							</p>
							<i class="icon icon_calendar"></i><span class="alt-font">Publicado el 1 De Mayo 2017</span><br>
							<i class="icon icon_clock_alt"></i><span class="alt-font">Lectura de 5 Minutos</span>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</div><!--end of blog-snippet-3-->

			<div class="blog-snippet-3">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<h1><a target="_blank" href="">Título Ejemplo con video de <em>Agenda 4</em> </a></h1>
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras at luctus erat. Aenean molestie, odio eu mattis feugiat, urna purus tincidunt augue, eu iaculis sapien eros eget lacus. Sed dapibus leo nec urna iaculis dignissim. Nullam iaculis in risus sit amet facilisis. Nunc convallis euismod dui sit amet sodales.
							</p>
							<i class="icon icon_calendar"></i><span class="alt-font">Publicado el 1 De Mayo 2017</span><br>
							<i class="icon icon_clock_alt"></i><span class="alt-font">Lectura de 4 Minutos</span>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</div><!--end of blog-snippet-3-->

		</section>

		<section class="clients-2">
			<div class="container">
				<div class="row">

					<div class="col-md-4 col-sm-4">
						<img alt="" src="{{ url('lideres/img/sponsor2.png') }}">
					</div>

					<div class="col-md-4 col-sm-4">
						<img alt="" src="{{ url('lideres/img/sponsor1.png') }}">
					</div>

					<div class="col-md-4 col-sm-4">
						<img alt="" src="{{ url('lideres/img/sponsor3.png') }}">
					</div>


				</div><!--end of row-->
			</div><!--end of container-->
		</section>

	</div>

@endsection

