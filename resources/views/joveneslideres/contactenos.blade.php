@extends('joveneslideres.master')
@section('content')

<div class="main-container">
	<section class="map-overlay">
		<div class="map-holder">
			<iframe src="
https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3549.8476279842157!2d-58.98858247011763!3d-27.449725366537862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94450cf504207af5%3A0x4dafd7bd43e84a76!2sMarcelo+T.+de+Alvear+145%2C+H3500ACD+Resistencia%2C+Chaco!5e1!3m2!1ses-419!2sar!4v1492897179045"></iframe>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
					<div class="details-holder">
						<div class="align-vertical">
							<img alt="Logo" src="{{ url('lideres/img/logo-contactenos.png') }}">
							<img alt="Logo" src="{{ url('lideres/img/logo-contactenos1.png') }}">
							<p class="lead">
								<font color="Teal">sec.empleoytrab@chaco.gov.ar</font><br>
								Tel: 062 4456452
							</p>
							<p>
								Marcelo T. de Alvear 145<br>
								Ciudad de Resistencia, Chaco.
							</p>
						</div>
					</div>
				</div>
			</div><!--end of row-->
		</div><!--end of container-->
	</section>

	<section class="clients-2">
		<div class="container">
			<div class="row">

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor2.png') }}">
				</div>

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor1.png') }}">
				</div>

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor3.png') }}">
				</div>


			</div><!--end of row-->
		</div><!--end of container-->
	</section>

	<!--<section class="pure-text-contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                    <span class="sub alt-font">We are accepting clients</span>
                    <h1><strong>We're located in Melbourne, Australia<br> and we work with clients globally.</strong></h1>
                    <i class="icon icon-map icon-jumbo"></i>
                    <i class="icon icon-bike icon-jumbo"></i>
                    <i class="icon icon-streetsign icon-jumbo"></i>
                </div>
            </div><!--end of row-->

</div><!--end of container-->

@endsection