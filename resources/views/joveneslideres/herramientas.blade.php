<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    
<head>
        <meta charset="utf-8">
        <title>Jóvenes Líderes Chaco | Herramientas</title>
        <meta name="description" content="El programa Jóvenes Líderes tiene como objetivo fomentar y generar el espíritu  de liderazgo en los jóvenes, potenciando sus capacidades humanas y profesionales.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="FEELWIT">
		<link rel="shortcut icon" type="image/png" href="../../../public/lideres/img/favicon.png"/>
		<link href="../../../public/lideres/css/flexslider.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/line-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
        <link href="../../../public/lideres/css/theme-aquatica.css" rel="stylesheet" type="text/css" media="all"/>
        <!--[if gte IE 9]>
        	<link rel="stylesheet" type="text/css" href="css/ie9.css" />
		<![endif]-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700' rel='stylesheet' type='text/css'>
        <script src="../../../public/lideres/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
    	<div class="loader">
    		<div class="spinner">
			  <div class="double-bounce1"></div>
			  <div class="double-bounce2"></div>
			</div>
    	</div>
				
		<div class="nav-container">
			<nav class="top-bar">
				<div class="container">
				
					<div class="row nav-menu">
						<div class="col-sm-2 col-md-2 columns">
							<a href="index.blade.php">
								<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo1.png">
							</a>
							
							<a href="index.blade.php">
								<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo2.png">
							</a>
							
						</div>
					
						<div class="col-sm-8 col-md-8 columns">
							<ul class="menu">
								<li><a href="index.blade.php">Inicio</a>
								<li><a href="financiamiento.blade.php">Financiamiento</a>
								<!--<li><a href="formacion.blade.php">Formación</a>-->
								<!--<li><a href="herramientas.html">Herramientas</a>-->
								<li><a href="agenda.blade.php">Agenda</a>
								<li><a href="inscripcion.blade.php">Inscripción</a>
								<li><a href="contactenos.blade.php">Contacto</a>
								
							</ul>
						
						</div>
						
						<div class="col-sm-2 col-md-2 columns">
							<a href="index.blade.php">
									<img class="logo logo-dark" alt="Logo" src="../../../public/lideres/img/logo.png">
								</a>
						</div>
					</div><!--end of row-->
					
					<div class="mobile-toggle">
						<i class="icon icon_menu"></i>
					</div>
					
				</div><!--end of container-->
			</nav>
		</div>
		
		<div class="main-container">
			<header class="signup">
					<div class="background-image-holder parallax-background">
						<img class="background-image" alt="Background Image" src="../../../public/lideres/img/imagen4.jpg">
					</div>
					
					<div class="container">
						<div class="row">
							<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
								<h1 class="text-white">Te ofrecemos distintas HERRAMIENTAS técnicas </br>para el armado de proyectos</h1>
							</div>
						</div><!--end of row-->
						
					</div><!--end of container-->	
			</header>
			
			<section class="feature-divider" id="7">
			
				<div class="background-image-holder" data-scroll-reveal="wait 0.2s then enter 200px from bottom over 0.3s">
					<img class="background-image" alt="Background Image" src="../../../public/lideres/img/grey-bg.jpg">
				</div>
			
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<h1>Desarrollo de APP para emprendedores</h1>
							<p class="lead">
								APLICACIÓN DE DESCARGA GRATUITA CON CONTENIDO PARA EMPRENDEDORES DE CHACO.
							</p>
							<p class="lead">
								- Calendario de eventos, talleres, etc. Contará también con una herramienta muy útil para gestionar reuniones y realizar conferencias.</br> - Permite al usuario grabar notas de voz o escritas de las ponencias a las que asiste gracias a su herramienta de transcripción de voz a texto.</br> - Será muy útil para aquellos que asistan a los talleres y conferencias.
							</p>
							<a class="store-link" href="#"><img alt="Buy On App Store" src="../../../public/lideres/img/app-store.png"></a>
							<a class="store-link" href="#"><img alt="Buy On App Store" src="../../../public/lideres/img/google-play.png"></a>
						</div>
					
						<div class="col-sm-7" data-scroll-reveal="enter from bottom and move 100px">
							<img alt="App Screenshot" src="../../../public/lideres/img/app2.png">
						</div>
					</div><!--end of row-->
				</div>
			</section>
			
			<section class="bg-primary">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							<div class="testimonials-slider text-center">
								<ul class="slides">
									<li>
										<p class="text-white lead">Aca se puede poner un caso o un texto comentando de alguien que descargo la APP o Utilizo la Plataforma</p>
										<span class="author text-white">Juan Perez - CASO 1</span>
									</li>
									
									<li>
										<p class="text-white lead">Aca se puede poner un caso o un texto comentando de alguien que descargo la APP o Utilizo la Plataforma</p>
										<span class="author text-white">Juan Perez - CASO 2</span>
									</li>
									
									<li>
										<p class="text-white lead">Aca se puede poner un caso o un texto comentando de alguien que descargo la APP o Utilizo la Plataforma</p>
										<span class="author text-white">Juan Perez - CASO 3</span>
									</li>
								</ul>
							</div>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			<section class="hero-divider" id="8">
			
				<div class="background-image-holder overlay">
					<img class="background-image" alt="Poster Image For Mobiles" src="../../../public/lideres/img/side1.jpg">
				</div>
			
				<div class="video-wrapper">
					<video autoplay="" muted="" loop="">
						<source src="../../../public/lideres/video/video.webm" type="video/webm">
						<source src="../../../public/lideres/video/video.mp4" type="video/mp4">
						<source src="../../../public/lideres/video/video.ogv" type="video/ogg">
					</video>
		
				</div>
				
				<div class="container">
					<div class="row">	
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
							<h1 class="text-white">Desarrollo de Web</h1>
							<p class="lead text-white">
								DESARROLLO DE PLATAFORMA WEB PARA EMPRENDEDORES DE CHACO
							</p>
							<p class="lead text-white">
								<strong>Objetivo:</strong> Conectar a freelancers de Chaco con posibles clientes del resto del país, la Región y el mundo para potenciar sus ventas y oferta de servicios.</br> A través de esta herramienta, los virtuales empleadores podrán contratar emprendedores locales para hacer trabajo en áreas tales como desarrollo de software, redacción, ingreso de datos y diseño hasta ingeniería, ciencias, ventas y marketing, servicios contables y legales.

							</p>
							<a href="#" class="btn btn-primary btn-filled">VER PLATAFORMA</a>
						</div>
					</div><!--end of row-->
				</div><!--end of row-->
			</section>
			
			<section class="clients-2">
				<div class="container">
					<div class="row">
												
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor2.png">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor1.png">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="../../../public/lideres/img/sponsor3.png">
						</div>
						
						
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
			
			
		</div>
		
		<div class="footer-container">
			
		
			<footer class="short bg-secondary-1">
				<div class="container">
					<div class="row">
						<div class="col-sm-10">
							<span class="sub">© 2017 JOVENES LIDERES CHACO.</span>
							
						</div>
						
						<div class="col-sm-2 text-right">
							<ul class="social-icons">
								<li>
									<a target="_blank" href="https://twitter.com/SecEmpleoChaco">
										<i class="icon social_twitter"></i>
									</a>
								</li>
								
								<li>
									<a target="_blank" href="https://web.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547">
										<i class="icon social_facebook"></i>
									</a>
								</li>
							</ul>	
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</footer>
		</div>
				
		<script src="../../../public/lideres/js/jquery.min.js"></script>
        <script src="../../../public/lideres/js/jquery.plugin.min.js"></script>
        <script src="../../../public/lideres/js/bootstrap.min.js"></script>
        <script src="../../../public/lideres/js/jquery.flexslider-min.js"></script>
        <script src="../../../public/lideres/js/smooth-scroll.min.js"></script>
        <script src="../../../public/lideres/js/skrollr.min.js"></script>
        <script src="../../../public/lideres/js/spectragram.min.js"></script>
        <script src="../../../public/lideres/js/scrollReveal.min.js"></script>
        <script src="../../../public/lideres/js/isotope.min.js"></script>
        <script src="../../../public/lideres/js/twitterFetcher_v10_min.js"></script>
        <script src="../../../public/lideres/js/lightbox.min.js"></script>
        <script src="../../../public/lideres/js/jquery.countdown.min.js"></script>
        <script src="../../../public/lideres/js/scripts.js"></script>
    </body>

</html>
				