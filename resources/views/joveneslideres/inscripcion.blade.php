@extends('joveneslideres.master')
@section('content')
<div class="main-container">

	<section class="contact-center">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
					<h1>CONTANOS TU PROYECTO</h1>
					<p class="lead">
						Si tenés alguna idea en desarrollo o a desarrollar,  envianos tu proyecto para evaluarlo.
					</p>
				</div>
			</div><!--end of row-->

			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="form-wrapper clearfix">
						<form class="form-contact email-form">
							<div class="inputs-wrapper">
								<input class="form-name validate-required" type="text" placeholder="Nombre y apellido" name="name">
								<input class="form-name validate-required" type="text" placeholder="Domicilio" name="domicilio">
								<input class="form-name validate-required" type="text" placeholder="Provincia" name="provincia">
								<select class="form-control validate-required" type="text" name="localidad">
									<option value="0" selected="selected">Seleccionar Localidad:</option>
									<option value="1">Resistencia</option>
									<option value="2">Puerto Tirol</option>
									<option value="3">Barranquereras</option>
									<option value="4">Fontana</option>
									<option value="5">Puerto Vilelas</option>
									<option value="6">Presidencia Roque S&aacute;enz Pe&ntilde;a</option>
									<option value="7">Charata</option>
									<option value="8">General Jos&eacute; de San Mart&iacute;n</option>
									<option value="9">Juan Jos&eacute; Castelli</option>
									<option value="10">Las Bre&ntilde;as</option>
									<option value="11">Quitilipi</option>
									<option value="12">Villa &Aacute;ngela</option>
									<option value="13">Avi&aacute; Tera&iacute;</option>
									<option value="14">Campo Largo</option>
									<option value="15">Concepci&oacute;n del Bermejo</option>
									<option value="16">Coronel Du Graty</option>
									<option value="17">Corzuela</option>
									<option value="18">General Pinedo</option>
									<option value="19">Hermoso Campo</option>
									<option value="20">La Escondida</option>
									<option value="21">La Leonesa</option>
									<option value="22">Las Palmas</option>
									<option value="23">Makall&eacute;</option>
									<option value="24">Miraflores</option>
									<option value="25">Pampa del Indio</option>
									<option value="26">Pampa del Infierno</option>
									<option value="27">Presidencia de la Plaza</option>
									<option value="28">Puerto Tirol</option>
									<option value="29">Puerto Vilelas</option>
									<option value="30">San Bernardo</option>
									<option value="31">Taco Pozo</option>
									<option value="32">Tres Isletas</option>
									<option value="33">Villa Berthet</option>
									<option value="34">Basail</option>
									<option value="35">Capit&aacute;n Solari</option>
									<option value="36">Charadai</option>
									<option value="37">Chorotis</option>
									<option value="38">Ciervo Petiso</option>
									<option value="39">Colonia Ben&iacute;tez</option>
									<option value="40">Colonia Elisa</option>
									<option value="41">Colonia Popular</option>
									<option value="42">Colonias Unidas</option>
									<option value="43">Cote Lai</option>
									<option value="44">El Espinillo</option>
									<option value="45">El Sauzalito</option>
									<option value="46">Enrique Uri&eacute;n</option>
									<option value="47">Fuerte Esperanza</option>
									<option value="48">Gancedo</option>
									<option value="49">General Capdevila</option>
									<option value="50">General Vedia</option>
									<option value="51">Isla del Cerrito</option>
									<option value="52">La Clotilde</option>
									<option value="53">La Eduvigis</option>
									<option value="54">La Tigra</option>
									<option value="55">La Verde</option>
									<option value="56">Laguna Blanca</option>
									<option value="57">Laguna Limpia</option>
									<option value="58">Lapachito</option>
									<option value="59">Las Garcitas</option>
									<option value="60">Los Frentones</option>
									<option value="61">Margarita Bel&eacute;n</option>
									<option value="62">Misi&oacute;n Nueva Pompeya</option>
									<option value="63">Napenay</option>
									<option value="64">Pampa Almir&oacute;n</option>
									<option value="65">Presidencia Roca</option>
									<option value="66">Puerto Bermejo</option>
									<option value="67">Puerto Eva Per&oacute;n</option>
									<option value="68">Samuh&uacute;</option><option value="69">Villa R&iacute;o Bermejito</option></select>
								<input class="form-name validate-required" type="date" placeholder="Fecha de nacimiento" name="fechanacimiento">
								<input class="form-name validate-required" type="text" placeholder="Lugar de Nacimiento" name="lugarnacimiento">
								<input class="form-name validate-required" type="text" placeholder="Sexo" name="sexo">
								<input class="form-name validate-required" type="text" placeholder="Estado civil" name="estadocivil">
								<input class="form-name validate-required" type="number" placeholder="D.N.I." name="dni">
								<input class="form-name validate-required" type="tel" placeholder="Teléfono" name="telefono">
								<input class="form-email validate-required validate-email" type="text" name="email" placeholder="E-mail">
								<select class="form-control" name="nivel_educativo">
									<option value="0" selected="selected">Nivel de educación:</option>
									<option value="1">Secundario</option>
									<option value="2">Terciario</option>
									<option value="3">Universitario</option>
									<option value="4">Posgrado</option>
									<option value="5">Master</option>
									<option value="6">Doctorado</option>
									<option value="7">Otro</option></select>
								<input class="form-name validate-required" type="text" placeholder="Ocupación" name="ocupacion">
								<input class="form-name validate-required" type="text" placeholder="Profesión" name="profesion">
								<textarea class="form-message validate-required" name="message" placeholder="Descripción en caso de seleccionar si"></textarea>
							</div>
							<input type="submit" class="send-form" value="ENVIAR">
							<div class="form-success">
								<span class="text-white">Mensaje enviado - Gracias por su proyecto</span>
							</div>
							<div class="form-error">
								<span class="text-white">Por favor complete todos los campos correctamente</span>
							</div>
						</form>
					</div>
				</div>
			</div><!--end of row-->
		</div><!--end of container-->
	</section>

	<section class="clients-2">
		<div class="container">
			<div class="row">

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor2.png') }}">
				</div>

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor1.png') }}">
				</div>

				<div class="col-md-4 col-sm-4">
					<img alt="" src="{{ url('lideres/img/sponsor3.png') }}">
				</div>


			</div><!--end of row-->
		</div><!--end of container-->
	</section>



</div>
@endsection
