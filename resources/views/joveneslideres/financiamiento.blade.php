@extends('joveneslideres.master')
@section('content')
	<div class="main-container">
			<header class="page-header resume-header">
				<div class="background-image-holder parallax-background">
					<img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen3.jpg') }}">
				</div>
				
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h1 class="text-white space-bottom-medium">FINANCIAMIENTO</h1>
							<span>"Crédito Jóven"</span>
							
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
			</header>
			
			<section class="duplicatable-content milestones">
			
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
							<h1>Documentación y Requisitos</h1>
							<!--<p class="lead">
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae. 
							</p>-->	
						</div>
					</div><!--end of row-->
		
					<div class="row">
						<div class="col-md-3 col-sm-6 text-center">
							<div class="feature feature-icon-large">
								<i class="icon icon_check"></i>
								<div class="pin-body"></div>
								<div class="pin-head"></div>
								<h5>Los beneficiarios </h5>
								<span>Serán aquellos </span>
								<span class="sub">que tengan un proyecto aprobado por el programa Jóvenes Líderes.</span>
								<div class="pin-body"></div>
								<div class="pin-head"></div>
								<h5>La entrega de </h5>
								<span>documentación será recibida</span>
								<span class="sub">del 1 al 10 de cada mes  indefectiblemente (SIN EXCEPCIÓN), pasada la fecha, ingresará el mes que le suceda en el calendario.</span>
								
							</div>
						</div><!--end 3 col-->
						
						<div class="col-md-3 col-sm-6 text-center">
							<div class="feature feature-icon-large">
								<i class="icon icon_check"></i>
								<div class="pin-body"></div>
								<div class="pin-head"></div>
								<h5>Presentación de carpeta y documentación obligatoria </h5>
								<span>(en carpeta tipo colgante).</span>
								<span class="sub">-	DNI del solicitante (no podrá ser solicitante quien perciba pensiones no contributivas, por incapacidad).
</br>-	Última boleta pagada de luz, agua, teléfono fijo o video cable del domicilio real del solicitante y/o del local donde se desarrollará el emprendimiento.
</br>-	Constancia de inscripción en AFIP, y ATP, debiendo coincidir la actividad inscripta con la denuncia en el proyecto.
</br>-	CBU (Expedido por el Nuevo Banco del Chaco)- Excluyente-
</br>-	Recibo de sueldo de un garante de $ 15 mil en adelante (sujeto a monto del crédito).</span>
							</div>
						</div><!--end 3 col-->
						
						<div class="col-md-3 col-sm-6 text-center">
							<div class="feature feature-icon-large">
								<i class="icon icon_check"></i>
								<div class="pin-body"></div>
								<div class="pin-head"></div>
								<h5>Adjuntar</h5>
								<span>Modelo de Pre- Proyecto Productivo o Servicio.</span>
								<span class="sub">-	Fundamentación </br>(por qué se quiere hacer).
</br>-	Descripción de Proyecto </br>(qué se quiere hacer).
</br>-	Justificación </br>(cómo y dónde se lo va a hacer). 
</span>
							</div>
						</div><!--end 3 col-->
						
						<div class="col-md-3 col-sm-6 text-center">
							<div class="feature feature-icon-large">
								<i class="icon icon_box-checked"></i>
								<div class="pin-body"></div>
								<div class="pin-head"></div>
								<h5>TODA carpeta </h5>
								<span>presentada indebidamente o INCOMPLETA</span>
								<span class="sub">quedará fuera del circuito de aprobación.</span>
							</div>
						</div><!--end 3 col-->
					
		
					</div><!--end of row-->
				</div>
			
			</section>
			
			<section class="projects-gallery">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
							<h3>¿TENÉS ALGUNA DUDA ACERCA DE LAS CONDICIONES PARA RECIBIR EL FINANCIAMIENTO? Comunícate con nosotros</h3>
							<p class="lead">
								<strong></br>Subsecretaria de Economía Social</strong>
							</p>
							<p class="lead">
								<strong>E-mail:</strong> certificacionchacoproduce@gmail.com </br><strong>Tel:</strong> 062 4438761  </br><strong>Ubicación:</strong> Sarmiento y Marcelo T. de Alvear 
							</p>
						</div>
					</div><!--end of row-->
				</div><!--end of container-->
				
			</section>
			
			<section class="stat-counters duplicatable-content">
			
				<div class="background-image-holder parallax-background">
					<img class="background-image" alt="Background Image" src="{{ url('lideres/img/imagen6.jpg') }}">
				</div>
		
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
							<h1>Acompañando tu crecimiento</h1>	
						</div>
					</div><!--end of row-->
					
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="stat feature">
								<div class="stat-bubble">
									<span>+ 95</span>
								</div>
								<h3>Proyectos Presentados</h3>	
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<div class="stat feature">
								<div class="stat-bubble">
									<span>+ 84</span>
								</div>
								<h3>Proyectos Aprobados</h3>	
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<div class="stat feature">
								<div class="stat-bubble">
									<span>+ 123</span>
								</div>
								<h3>Nuevos Emprendedores</h3>	
							</div>
						</div>
						
						<div class="col-md-3 col-sm-6">
							<div class="stat feature">
								<div class="stat-bubble">
									<span>+ 245</span>
								</div>
								<h3>Nuevos Puestos de Trabajo</h3>	
							</div>
						</div>
						
					</div><!--end of row-->
		
				</div><!--end of container-->
			</section>
			
			<section class="clients-2">
				<div class="container">
					<div class="row">
												
						<div class="col-md-4 col-sm-4">
							<img alt="" src="{{ url('lideres/img/sponsor2.png') }}">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="{{ url('lideres/img/sponsor1.png') }}">
						</div>
						
						<div class="col-md-4 col-sm-4">
							<img alt="" src="{{ url('lideres/img/sponsor3.png') }}">
						</div>
						
						
					</div><!--end of row-->
				</div><!--end of container-->
			</section>
		</div>
@endsection

				