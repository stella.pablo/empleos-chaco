@extends('template.admin_template')

@section('titulo')
    Editar Empresa
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Proveedores</a></li>
        <li class="active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($empresa, array('method' => 'PUT', 'route' => array('admin.empresas.update', $empresa->id), 'class' => 'form-horizontal', 'files'=>'true')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Razon Social</label>
                        <div class="col-sm-6">
                            {!! Form::text('razon_social', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Area</label>
                        <div class="col-sm-6">
                            {!! Form::select('area_id', $areas ,  null, ['class' => 'form-control col-sm-4'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">CUIT</label>
                        <div class="col-sm-4">
                            {!! Form::text('cuit', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-6">
                            {!! Form::text('direccion', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-4">
                            {!! Form::text('telefono', null, ['class' => 'form-control','placeholder'=>'0000-0000'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descripción de la empresa</label>
                        <div class="col-sm-9">
                            {!! Form::textarea('sinopsis', null, ['class' => 'form-control','rows' =>'5'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Logo</label>
                        <div class="col-sm-6">
                            {!! Form::file('logo') !!}
                        </div>
                    </div>
                    @if($empresa->logo != NULL)
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                               <img src="{{ url('images/empresas/',$empresa->logo) }}" />
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            {!! Form::text('email', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancelar</button>
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
@endsection