@extends('template.admin_template')

@section('titulo')
    Editar Area
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Areas</a></li>
        <li class="active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($area, array('method' => 'PUT', 'route' => array('admin.areas.update', $area->id), 'class' => 'form-horizontal')) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            {!! Form::text('nombre', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Icono</label>
                        <div class="col-sm-4">
                            {!! Form::text('icono', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancelar</button>
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
@endsection