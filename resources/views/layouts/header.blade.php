<header id="header">

    <!--BURGER NAVIGATION SECTION START-->

    <div class="cp-burger-nav">
    </div>

    <!--BURGER NAVIGATION SECTION END-->

    <div class="container">

        <!--NAVIGATION START-->

        <div class="navigation-col">

            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

                    <strong class="logo">
                        <a href="{{ url('/') }}">
                        <img src="{{ url('images/logo.png') }}" alt="logo" width="320px">
                        </a>
                    </strong>
                    
                </div>  

                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav" id="nav">
                        @if(auth()->user())
                        <li>
                            <a href="{{ url('postulantes/create') }}">Ver CV</a>
                        </li>
                        @else
                        <li>
                            <a href="{{ url('login') }}">Carga tu CV</a>
                        </li>
                        @endif

                        <li><a href="{{ url('guia/emprendedores') }}">Emprendedores</a></li>
                        <li><a href="{{ url('guia/oficios') }}">Oficios</a></li>
                        <li><a href="{{ url('blog') }}">Noticias</a></li>
                        <li><a href="#">Mapa</a>
                                <ul>
                                    <li><a href="{{ url('registro') }}">Registrar puesto</a></li>
                                    <li><a href="{{ url('mapa') }}">Mapa de empleo</a></li>

                                </ul>

                        </li>
                        <li><a href="{{ url('contacto') }}">Contáctanos</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <!--NAVIGATION END-->

    </div>

    <!--USER OPTION COLUMN START-->

    <div class="user-option-col">

        @if(Auth::user())

        <div class="thumb">

            <div class="dropdown">

                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <img src="{{ url('images/user-img.png') }}" alt="img">
                </button>

                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                    <li><a href="{{ url('postulantes/create') }}">Editar mi CV</a></li>
                    <li><a href="{{ url('/logout') }}">Salir</a></li>                    

                </ul>

            </div>

        </div>

        @endif

    </div>

    <!--USER OPTION COLUMN END-->

</header>