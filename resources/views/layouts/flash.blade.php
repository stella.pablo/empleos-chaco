@if (session()->has('flash_message'))
    <div class="callout callout-{{ session('flash_message_level') }}">
        <h4>{{ session('flash_message') }}</h4>
    </div>
@endif
