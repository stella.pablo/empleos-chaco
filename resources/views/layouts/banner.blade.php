
<div class="banner-outer">

    <div id="banner" class="element"> <img src="{{ url('images/banner-img-1.png') }}" alt="banner"> </div>

    <div class="caption">

        <div class="holder">
 
            <h1>Empleo en Chaco</h1>

            <div class="btn-row">
                <a href="{{ url('empresas') }}"><i class="fa fa-building-o"></i>Buscar personal</a>
                <a href="{{ url('guia','oficios') }}"><i class="fa fa-user "></i>OFICIOS</a>
                <a href="{{ url('guia','emprendedores') }}"><i class="fa fa-user "></i>EMPRENDEDORES</a>
            </div>

            {!! Form::open(array('route' => 'busquedas')) !!}

            <div class="container">

                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <input type="text" name="parametro" placeholder="Escribe cargo o área profesional">
                    </div>

                    <div class="col-md-4 col-sm-4">
                        {!! Form::select('localidad_id', $localidades ,  null)!!}
                    </div>

                    <div class="col-md-4 col-sm-4">
                        {!! Form::select('area_id',  $areas ,  null)!!}
                    </div>

                    <div class="col-md-1 col-sm-1">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>

            {{ Form::close() }}


        </div>

    </div>

    <div class="browse-job-section">

        <div class="container">

            <div class="holder"> 
            @if (Auth::user())
                <strong class="title">
                    Encuentra el candidato que necesitas de la forma más rápida, fácil y segura. 
                </strong> 
                @else
                <a href="{{ url('login') }}" class="btn-brows">Registrarte</a> 
                <strong class="title">
                    ¿Buscás trabajo? Regístrate y accede a todas las ofertas laborales del Sitio. 
                </strong>                 
                @endif
            </div>

        </div>

    </div>

</div>

<!--BANNER END