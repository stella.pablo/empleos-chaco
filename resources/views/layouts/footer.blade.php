<!--FOOTER START-->

<footer id="footer">

    <div class="container">
        <div class="bottom-row"> 
        
        <strong class="copyrights">
        <img src="{{ url('images/footer-gobernacion.png') }}" width="150px" alt="">
        Secretaría de Empleo y trabajo (Provincia del Chaco) 2016 | Gobierno del Pueblo del Chaco</strong>
            <div class="footer-social">
                <ul>
                    <li><a href="https://twitter.com/LiliSpoljaric" target="_new"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/Secretar%C3%ADa-de-Empleo-y-Trabajo-625076337592547/?ref=ts&fref=ts" target="_new"><i class="fa fa-facebook-f"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

<!--FOOTER END-->