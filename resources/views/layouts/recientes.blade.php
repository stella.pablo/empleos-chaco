<!--RECENT JOB SECTION START-->

<section class="recent-row padd-tb">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div id="content-area">
                    <h2>Empleos a los que puédes postularte</h2>

                    <ul id="myList">
                        @foreach($ofertas as $row)
                            @if($row->urgente == 1)
                            <div class="box red">
                                <div class="text-col">
                                    <div class="tags">
                                        <i class="fa fa-tags"></i> Se precisa URGENTE
                                    </div>
                                    @else
                                    <div class="box">
                                        <div class="text-col">
                                            @endif
                                            <h4><a href="{{ route('puesto.show',$row->slug) }}">{{ $row->descripcion_corta }}</a></h4>
                                            <p>{{ \App\Area::find($row->area_id)->nombre }}</p>
                                            <p>{{ \App\Empresa::find($row->empresa_id)->razon_social }}</p>
                                            <a href="#" class="text">
                                                <i class="fa fa-map-marker"></i>{{ getUbicacion($row->provincia_id,$row->localidad_id) }}
                                            </a>
                                            @if($row->fecha_inicio != NULL)
                                                <a href="#" class="text">
                                                    <i class="fa fa-calendar"></i>{{ $row->fecha_inicio->format('d/m/Y') }}
                                                </a>
                                            @endif
                                        </div>
                                        <a href="{{ route('puesto.show',$row->slug) }}" class="btn-1 btn-color-{{ getColor($row->modalidad) }} ripple">{{ $row->modalidad }}</a>
                                    </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>

            <div class="col-md-3 col-sm-4">
                <aside>

                    <div class="sidebar">

                        <div class="sidebar-jobs">
                            <ul>
                                <li>
                                    <a href="{{ url('joveneslideres') }}"><img width="265px;" src="{{ url('images/lideres.jpg') }}" /></a>
                                </li>
                                @foreach($blog as $row)
                                <li>
                                    <a href="{{ route('guia.blog.post',$row->slug)  }}">{{ $row->titulo }}</a>
                                        <span><i class="fa fa-calendar"></i>{{ $row->created_at->format('d/m/Y') }}</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="col-md-3 col-sm-4">
                        <aside style="margin-top: 30px;">
                            <div class="sidebar">
                                @foreach($destacado as $row)
                                <div class="box">
                                    <div class="text-box">
                                        <a href="#" class="btn-time">{{ $row->modalidad }}</a>
                                        <h4><a href="{{ route('puesto.show',$row->slug) }}">{{ $row->descripcion_corta }}</a></h4>
                                        <a href="#" class="text"><i class="fa fa-map-marker"></i>{{ getUbicacion($row->provincia_id,$row->localidad_id) }}</a> <a href="#" class="text"><i class="fa fa-calendar"></i>{{ $row->fecha_inicio->format('d/m/Y') }}</a>
                                    </div>
                                </div>
                                @endforeach
                            </div>


                            <a href="{{ url('contacto') }}"><img width="265px;" src="{{ url('images/banner01.jpg') }}" /></a>
                            <img width="265px;" src="{{ url('images/banner03.jpg') }}" />
                        </aside>

                    </div>
                </div>
            </div>

        </section>

<!--RECENT JOB SECTION END-->