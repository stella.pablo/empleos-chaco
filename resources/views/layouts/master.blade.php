<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Empleo y Trabajo - Chaco</title>
    @yield('style')
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/color.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/responsive.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>

    <link href="{{ url('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ url('css/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500,700,900' rel='stylesheet' type='text/css'>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body class="theme-style-1">
<div id="wrapper">

    @include('layouts.header')

    @yield('cabecera')

    <!--MAIN START-->

    <div id="main">

        @yield('content')


    </div>

    <!--MAIN END-->



    @include('layouts.footer')

</div>

<!--WRAPPER END-->
<script src="{{ url('js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/owl.carousel.min.js') }}"></script>
<script src="{{ url('js/jquery.velocity.min.js') }}"></script>
<script src="{{ url('js/jquery.kenburnsy.js') }}"></script>
<script src="{{ url('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ url('js/custom.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>

@yield('script')
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-80061713-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
