@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <section id="inner-banner">
        <div class="container">
            <h1>Registrate! para cargar tu CV </h1>
        </div>
    </section>

    <section class="signup-section">
        <div class="container">
            <div class="holder">
                <div class="thumb"><img src="{{ url('images/signup.png') }}" alt="img"></div>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {!! csrf_field() !!}

                    <div class="input-box"> <i class="fa fa-envelope-o"></i>
                        <input type="text" placeholder="Email" name="email" value="{{ old('email') }}">
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="input-box"> <i class="fa fa-unlock"></i>
                        <input type="password" placeholder="Password" name="password">
                    </div>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                    <div class="input-box"> <i class="fa fa-unlock"></i>
                        <input type="password" placeholder="Confirmar Password" name="password_confirmation">
                    </div>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                    <input type="submit" value="Registrar">

                </form>
            </div>
        </div>

    </section>

@endsection
