@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <section id="inner-banner">
        <div class="container">
            <h1>Gestión Empleos 1.0</h1>
        </div>
    </section>

    <section class="signup-section">
        <div class="container">
            <div class="holder">
                @include('layouts.flash_front')
                <div class="thumb"><img src="{{ url('images/signup.png') }}" alt="img"></div>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/gestion/authenticate') }}">
                    {!! csrf_field() !!}

                    <div class="input-box"> <i class="fa fa-envelope-o"></i>
                        <input type="text" placeholder="Email" name="email" value="{{ old('email') }}">
                    </div>
                    @if ($errors->has('email'))
                        <span class="help_login">
                                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <div class="input-box"> <i class="fa fa-unlock"></i>
                        <input type="password" placeholder="Password" name="password">
                    </div>

                    @if ($errors->has('password'))
                        <span class="help_login">
                                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <input type="submit" value="Ingresar">

                </form>
            </div>
        </div>

    </section>
@endsection

