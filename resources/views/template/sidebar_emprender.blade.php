<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="active"><a href="{{ url('admin/emprendedores')  }}">
                    <i class="fa fa-fw fa-empire "></i> <span>Emprendedores</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/categorias-emprendedores')  }}">
                    <i class="fa fa-fw fa-file "></i> <span>Categoria Emprendimientos</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/consultas')  }}">
                    <i class="fa fa-fw fa-mail-forward "></i> <span>Consultas y Mensajes</span>
                </a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
