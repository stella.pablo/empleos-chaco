<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="active"><a href="{{ url('admin/blog')  }}">
                    <i class="fa fa-fw fa-newspaper-o "></i> <span>Noticias</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/empresas')  }}">
                    <i class="fa fa-fw fa-bookmark "></i> <span>Empresas</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/ofertas')  }}">
                    <i class="fa fa-fw fa-file-text "></i> <span>Ofertas</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/areas')  }}">
                    <i class="fa fa-fw fa-file-text "></i> <span>Areas</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/postulantes')  }}">
                    <i class="fa fa-fw fa-user "></i> <span>Postulantes</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/emprendedores')  }}">
                    <i class="fa fa-fw fa-empire "></i> <span>Emprendedores</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/oficios')  }}">
                    <i class="fa fa-fw fa-building-o "></i> <span>Oficios</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/postulados')  }}">
                    <i class="fa fa-fw fa-users "></i> <span>Postulados a ofertas</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/geolocalizacion')  }}">
                    <i class="fa fa-fw fa-users "></i> <span>Mapa</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/categorias-emprendedores')  }}">
                    <i class="fa fa-fw fa-file "></i> <span>Categoria Emprendimientos</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/categorias-oficios')  }}">
                    <i class="fa fa-fw fa-file-archive-o "></i> <span>Categoria Oficios</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/consultas')  }}">
                    <i class="fa fa-fw fa-mail-forward "></i> <span>Consultas y Mensajes</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/usuarios')  }}">
                    <i class="fa fa-fw fa-user-secret "></i> <span>Usuarios y perfiles</span>
                </a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
