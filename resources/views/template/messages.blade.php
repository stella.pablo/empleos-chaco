@if(Session::get('msg'))
    <div class="col-md-10">
        <div class="callout callout-success">
            <h4>{{ Session::get('msg') }}</h4>
        </div>
    </div>
@endif
@if(session()->has('stock'))
    <div class="col-md-10">
        <div class="callout callout-danger">
            <p>Los siguientes productos se encuentran con STOCK NEGATIVO</p>
            <ul>
                   @foreach(session('stock') as $row)
                       <li>{{ $row }}</li>
                   @endforeach
            </ul>
        </div>
    </div>
@endif