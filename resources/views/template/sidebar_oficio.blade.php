<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="active"><a href="{{ url('admin/oficios')  }}">
                    <i class="fa fa-fw fa-building-o "></i> <span>Oficios</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/categorias-oficios')  }}">
                    <i class="fa fa-fw fa-file-archive-o "></i> <span>Categoria Oficios</span>
                </a>
            </li>
            <li class="active"><a href="{{ url('admin/consultas')  }}">
                    <i class="fa fa-fw fa-mail-forward "></i> <span>Consultas y Mensajes</span>
                </a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
