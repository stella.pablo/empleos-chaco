@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <section id="inner-banner">

        <div class="container">
            <h1>Postulantes</h1>
        </div>

    </section>

    <section class="resum-form padd-tb">

        <div class="container">

            <div class="row bs-wizard" style="border-bottom:0;">

                <div class="col-xs-3 bs-wizard-step complete">
                    <div class="text-center bs-wizard-stepnum"><a href="{{ url('postulantes/create') }}">Datos de contacto</a></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                    <div class="text-center bs-wizard-stepnum"><a href="{{ url('postulantes/educacion') }}">Educación</a></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-3 bs-wizard-step complete">
                    <div class="text-center bs-wizard-stepnum">
                        <span class="contorno"><a href="{{ url('postulantes/experiencias_laborales') }}">Experiencia Laboral</a></span>
                    </div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

                <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                    <div class="text-center bs-wizard-stepnum"><a href="{{ url('postulantes/cursos') }}">Cursos</a></div>
                    <div class="progress"><div class="progress-bar"></div></div>
                    <a href="#" class="bs-wizard-dot"></a>
                </div>

            </div>

            @include('layouts.flash_front')

            <div class="col-md-12">
                <div class="btn-col">
                    <h4>Experiencias Laborales</h4>
                    <div id="content-area">

                        <ul id="myList">
                            @foreach($listado as $row)
                                <li style="display: list-item;">
                                    <div class="box">
                                        <div class="text-col">
                                            <h4>{{ $row->empresa }}, {{ $row->puesto }}</h4>
                                            <a href="#" class="text"><i class="fa fa-map-marker"></i>{{ getUbicacion($row->provincia_id,$row->localidad_id) }}</a> <a href="#" class="text"><i class="fa fa-calendar"></i>{{ $row->fecha_inicio }} - {{ $row->fecha_finalizacion }}</a>
                                        </div>
                                        <a href="" class="btn-1 btn-color-1">Eliminar</a>
                                        <a href="{{ route('postulantes.edit.experiencias_laborales',$row->id) }}" class="btn-1 btn-color-2">Editar</a>

                                    </div>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>



            <div class="col-md-12">
                <div class="btn-col">  </div>
                <h4>Ingresar nueva / Editar </h4>
                <div class="box box-info">
                    <div class="box-header with-border">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                                @foreach ( $errors->all() as $error )
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
                </br>
                {!! Form::open(array('route' => 'postulantes.store.experiencia', 'class' => 'form-horizontal')) !!}

                <div class="row">

                    <div class="form-group">

                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-5 control-label">Empresa</label>
                        <div class="col-sm-6">
                            {!! Form::text('empresa', null)!!}
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-2 control-label">Puesto</label>
                        <div class="col-sm-6">
                            {!! Form::text('puesto', null)!!}
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-5 control-label">Provincia</label>
                        <div class="col-sm-6">
                            {!! Form::select('provincia_id', $provincias, null, ['class'=>'form-control'])!!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-2 control-label">Localidad</label>
                        <div class="col-sm-6">
                            {!! Form::select('localidad_id', $localidades, null, ['class'=>'form-control'])!!}
                        </div>
                    </div>

                </div>
                </br>

                <div class="form-group">

                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-5 control-label">Area</label>
                        <div class="col-sm-7">
                            {!! Form::select('area_id', $areas, null, ['class'=>'form-control'])!!}
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-6">
                            {!! Form::text('direccion', null)!!}
                        </div>
                    </div>


                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-5 control-label">Fecha Inicio</label>
                        <div class="col-sm-6">
                            {!! Form::text('fecha_inicio', null, ['placeholder'=> 'dd/mm/aa','id'=>'datepicker2'])!!}
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <label class="col-sm-2 control-label">Fecha Fin</label>
                        <div class="col-sm-6">
                            {!! Form::text('fecha_finalizacion', null , ['placeholder'=> 'dd/mm/aa','id'=>'datepicker'])!!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-sm-10">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-7">
                            {!! Form::textarea('descripcion', null , ['placeholder'=> 'Descripción de las tareas que realizo'])!!}
                        </div>
                    </div>
                </div>


                    <div class="col-lg-offset-8">
                        <input style="margin: 3px" type="submit" class="guardar" value="Guardar">
                    </div>



                </div>

                {{ Form::close() }}
            </div>
        </div>

    </section>

@endsection
@section('script')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2016'
            });
            $( "#datepicker2" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2016'
            });
        });
    </script>

@endsection