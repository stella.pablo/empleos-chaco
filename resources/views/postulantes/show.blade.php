@extends('template.admin_template')

@section('titulo')
    Ver Postulante
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Postulantes</a></li>
        <li class="active">Ver</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box box-info">
                <!-- form start -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Nombre y Apellido</label>
                        <div class="col-sm-6">
                          <p>{{ $postulante->nombre }} {{ $postulante->apellido }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">DNI</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->dni }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Direccion</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->direccion }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Fecha Nacimiento</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->fecha_nac }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Tel. Movil / Tel Fijo</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->telefono_movil }} / {{ $postulante->telefono_fijo }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Estado Civil</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->estado_civil }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Localidad</label>
                        <div class="col-sm-6">
                            <p>{{ $postulante->localidad_id }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-6">
                            <p>{{ \App\User::find($postulante->user_id)->email }}</p>
                        </div>
                    </div>
                    @if($postulante->cv != NULL)
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Descargar CV</label>
                            <div class="col-sm-6">
                                <a target="_blank" href="{{ url('cv_postulantes/'. $postulante->cv) }}">Download</a>
                            </div>
                        </div>
                    @endif

                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
@endsection