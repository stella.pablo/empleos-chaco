@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom.css')  }}" rel="stylesheet" type="text/css">
@endsection
@section('content')


<section id="inner-banner">

        <div class="container">
            <h1>Postulantes</h1>
        </div>

</section>

<section class="resum-form padd-tb">

    <div class="container">

        <div class="row bs-wizard" style="border-bottom:0;">

            <div class="col-xs-3 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum">
                    <span class="contorno"><a href="{{ url('postulantes/create') }}">Datos de contacto</a></span>
                </div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                <div class="text-center bs-wizard-stepnum">Educación</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
                <div class="text-center bs-wizard-stepnum">Experiencia Laboral</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                <div class="text-center bs-wizard-stepnum">Cursos</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
            </div>
        </div>

        <div class="box box-info">
            <div class="box-header with-border">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                        @foreach ( $errors->all() as $error )
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>


        {!! Form::open(array('route' => 'postulantes.store', 'class' => 'form-horizontal', 'files'=> 'true')) !!}

        <p>Ingresar tu currículum te permitirá postularte a todas las búsquedas de empleos que se publiquen en el sitio y, si así lo deseás, las empresas y consultoras que accedan a nuestra base de datos en busca de candidatos podrán consultarlo. El proceso de ingreso del currículum está dividido en pasos. Al finalizar la carga de tus datos presioná el botón "guardar" al final de la página antes de ir al paso siguiente. Los ítems marcados con un * son obligatorios.</p>

        @include('layouts.flash_front')

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Nombre</label>
                <div class="col-sm-6">
                    {!! Form::text('nombre')!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">Apellido</label>
                <div class="col-sm-6">
                    {!! Form::text('apellido')!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Email</label>
                <div class="col-sm-6">
                    {!! Form::text('email',auth()->user()->email, ['disabled'=>'disabled'])!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">Fecha de Nacimiento</label>
                <div class="col-sm-6">
                    {!! Form::text('fecha_nac', null, ['placeholder'=> 'dd/mm/aa','id'=>'datepicker'])!!}
                </div>
            </div>


            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Sexo</label>
                <div class="col-sm-6">
                    {!! Form::select('sexo', $sexo, null, ['class'=>'form-control'])!!}
                </div>
            </div>


            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">DNI</label>
                <div class="col-sm-6">
                    {!! Form::text('dni' )!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Estado civil</label>
                <div class="col-sm-6">
                    {!! Form::select('estado_civil', $estados, null, ['class'=>'form-control'])!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">Foto</label>
                <div class="upload-box">
                    <div class="hold">
                        <a href="#">Tamaño maximo 2MB</a>
                        <span class="btn-file">Buscar {!! Form::file('foto') !!}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Localidad</label>
                <div class="col-sm-6">
                    {!! Form::select('localidad_id', $localidades, null , ['class'=>'form-control'])!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">Dirección</label>
                <div class="col-sm-6">
                    {!! Form::text('direccion')!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Tel. fijo</label>
                <div class="col-sm-6">
                    {!! Form::text('telefono_fijo')!!}
                </div>

            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-3 control-label">Tel. móvil</label>
                <div class="col-sm-6">
                    {!! Form::text('telefono_movil')!!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <label class="col-sm-5 control-label">Adjuntar CV</label>
                <div class="col-sm-6">
                    {!! Form::file('cv') !!}
                </div>
            </div>

            </div>


        <div class="col-lg-offset-8">
            <input style="margin: 3px" type="submit" class="guardar" value="Guardar">
        </div>

        {{ Form::close() }}

    </div>
        </div>

    </div>

</section>

@endsection
@section('script')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2016'
            });
        });
    </script>

@endsection
