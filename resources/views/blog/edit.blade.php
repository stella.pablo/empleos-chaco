@extends('template.admin_template')

@section('titulo')
    Nuevo Articulo
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Articulos</a></li>
        <li class="active">Nuevo</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-md-12s'>
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Atencion!</h4>

                            @foreach ( $errors->all() as $error )
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($blog, array('method' => 'PUT', 'route' => array('admin.blog.update', $blog->id), 'class' => 'form-horizontal', 'files'=>'true')) !!}

                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Titulo</label>
                        <div class="col-sm-8">
                            {!! Form::text('titulo', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Bajada</label>
                        <div class="col-sm-8">
                            {!! Form::text('bajada', null, ['class' => 'form-control'])!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Estado</label>
                        <div class="col-sm-6">
                            {!! Form::select('estado_id', $estado ,  null, ['class' => 'form-control col-sm-4'])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Imagen</label>
                        <div class="col-sm-6">
                            {!! Form::file('imagen') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Cuerpo</label>
                        <div class="col-sm-10">
                            {!! Form::textarea('cuerpo', null, ['class' => 'form-control','id'=>'editor1'])!!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-default">Cancelar</button>
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    </div><!-- /.box-footer -->
                    </form>
                </div>
            </div><!-- /.col -->

        </div><!-- /.row -->
    </div>

@endsection
@section('recursos')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'yy-mm-dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $( "#datepicker2" ).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2016'
            });

        });
    </script>
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CKEDITOR.replace('editor1');
            $(".textarea").wysihtml5();
        });
    </script>



@endsection