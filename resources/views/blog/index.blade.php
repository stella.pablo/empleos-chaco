@extends('template.admin_template')

@section('titulo')
    Articulos
    <small>{{ $page_description or null }}</small>
@endsection

@section('ubicacion')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Articulos</a></li>
        <li class="active">Grid</li>
    </ol>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-2">
                <a href="{{ route('admin.blog.create') }}" class="btn btn-block btn-success">Nuevo</a>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div><!-- /.box-header -->
                    <div id="bodyContenido" >
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Fecha</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @include('layouts.flash')
                            @foreach($blog as $row)
                                <tr>
                                    <td>{{ $row->titulo }}</td>
                                    <td>{{ $row->created_at->format('d/m/Y') }}</td>
                                    <td>
                                        <a href="{{ route('admin.blog.edit',$row->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-fw fa-edit"></i></a>
                                        <a href="{{ route('admin.blog.destroy',$row->id) }}" onclick="return confirm('Seguro que desea eliminar el articulo ?')" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
@section('recursos')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "autoWidth": false,
                "oLanguage": {
                    "sSearch": "Filtro: ",
                    "sInfoEmpty": 'No hay registros que mostrar ',
                    "sInfo": 'Mostrando _END_ filas.',
                }
            });
        });
    </script>

@endsection