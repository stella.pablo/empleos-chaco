@extends('layouts.master')
@section('meta')
    <meta property="og:title" content="{{ $blog->titulo  }}" />
    <meta property="og:description" content="{{ url(Request::path()) }}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ url('images/articulos/'. $blog->imagen) }}" />
    <meta name="image" content="{{ url('images/articulos/'. $blog->imagen) }}">
@endsection

@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection



@section('content')

    <section id="inner-banner">

        <div class="container">

            <h1>Noticias</h1>

        </div>

    </section>

<section class="post-section blog-post">

    <div class="container">

        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="post-box">
                    <div class="frame"><img src="{{ url('images/articulos/'. $blog->imagen) }}" alt="img"></div>
                    <div class="text-box">
                        <div class="clearfix"> <strong class="date"><i class="fa fa-calendar"></i>Mar 17, 2016 </strong> </div>
                        <h4>{{ $blog->titulo }}</h4>
                        <p>{!! $blog->cuerpo !!}</p>
                    </div>
                </div>

            </div>
        </div>

    </div>

</section>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56964ab060ce6290" async="async"></script>

@endsection