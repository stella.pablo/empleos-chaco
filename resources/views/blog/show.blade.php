@extends('layouts.master')
@section('style')
    <link href="{{ url('css/custom2.css')  }}" rel="stylesheet" type="text/css">
@endsection

@section('content')


    <section class="post-section blog-post">

    <div class="container">

        <div class="row">

            @foreach($blog as $row)
                <div class="col-md-4 col-sm-6">

                    <div class="post-box">

                        <div class="frame"><a href="{{ route('guia.blog.post',$row->slug)  }}"><img src="{{ url('images/articulos/thumb/'.$row->imagen) }}" alt="img"></a></div>

                        <div class="text-box"> <strong class="date"><i class="fa fa-calendar"></i>{{ $row->created_at->format('d/m/Y') }}</strong>

                            <h4><a href="{{ route('guia.blog.post',$row->slug)  }}">{{ $row->titulo }}</a></h4>


                    </div>

                    </div>
                </div>

            @endforeach



        <!--PAGINATION SECTION START-->

                {{ $blog->links() }}

        <!--PAGINATION SECTION END-->



    </div>

</section>

@endsection