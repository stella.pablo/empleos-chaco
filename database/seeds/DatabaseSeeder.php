<?php

use Illuminate\Database\Seeder;
use \App\Avisogps;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        Avisogps::create(['nombre'=>'prueba 1',
            'cuit'=>'20121194878',
            'direccion'=>'Av. siempre viva 3453, Resistencia Chaco',
            'latitud'=>'-27.472415',
            'longitud'=>'-58.981942',
            'cantpuestos'=>'12',
            'puesto'=>'Serenos a tiempo completo',
            'detalles'=>'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
            'categoria'=>'1'
        ]);

        Avisogps::create(['nombre'=>'prueba 2',
            'cuit'=>'20121194878',
            'direccion'=>'Av. siempre viva 3453, Resistencia Chaco',
            'latitud'=>'-27.453419',
            'longitud'=>'-58.989226',
            'cantpuestos'=>'12',
            'puesto'=>'Serenos a tiempo completo',
            'detalles'=>'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
            'categoria'=>'1'
        ]);

        Avisogps::create(['nombre'=>'prueba 3',
            'cuit'=>'20121194878',
            'direccion'=>'Av. siempre viva 3453, Resistencia Chaco',
            'latitud'=>'-27.005691',
            'longitud'=>'-59.848958',
            'cantpuestos'=>'12',
            'puesto'=>'Serenos a tiempo completo',
            'detalles'=>'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system',
            'categoria'=>'1'
        ]);
    }
}
