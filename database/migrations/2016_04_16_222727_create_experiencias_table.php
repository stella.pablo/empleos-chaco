<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->integer('localidad_id')->index()->unsigned();
            $table->string('direccion');
            $table->string('puesto');
            $table->integer('area_id')->index()->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_finalizacion');
            $table->integer('postulante_id')->index()->unsigned();
            $table->text('descripcion');
            $table->timestamps();
            $table->softDeletes();

            //Area va una tabla!

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('experiencias');
    }
}
