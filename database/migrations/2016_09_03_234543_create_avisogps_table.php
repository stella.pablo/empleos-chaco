<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisogpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avisogps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', '100');
            $table->integer('cuit');
            $table->string('direccion');
            $table->string('latitud');
            $table->string('longitud');
            $table->integer('cantpuestos');
            $table->string('puesto');
            $table->string('detalles');
            $table->integer('categoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avisogps');
    }
}
