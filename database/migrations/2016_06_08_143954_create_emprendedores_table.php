<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprendedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprendedores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('responsable');
            $table->string('cuit');
            $table->string('email');
            $table->string('direccion');
            $table->string('telefono');
            $table->text('descripcion');
            $table->integer('categoria_emprendedor_id')->index()->unsigned();
            $table->string('logo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emprendedores');

    }
}
