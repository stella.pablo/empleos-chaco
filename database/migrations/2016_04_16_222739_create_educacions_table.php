<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educacions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nivel_educativo')->index()->unsigned();
            $table->string('titulo');
            $table->enum('estado', array('En curso', 'Graduado','Abandonado'));
            $table->string('institucion');
            $table->integer('provincia_id')->index()->unsigned();
            $table->integer('localidad_id')->index()->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_finalizacion');
            $table->integer('postulante_id')->index()->unsigned();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('educacions');
    }
}
