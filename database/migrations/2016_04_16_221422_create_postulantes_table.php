<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostulantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->integer('localidad_id')->index()->unsigned();
            $table->enum('sexo', array('F', 'M'));
            $table->enum('estado_civil', array('Soltero/a', 'Casado/a','Viudo/a','Divorciado/a'));
            $table->string('dni');
            $table->string('direccion');
            $table->string('telefono_movil');
            $table->string('telefono_fijo');
            $table->string('foto');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('postulantes');
    }
}
