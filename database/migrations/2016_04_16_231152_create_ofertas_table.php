<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('puestos');
            $table->string('descripcion_corta');
            $table->text('descripcion_larga');
            $table->date('fecha_inicio');
            $table->date('fecha_finalizacion');
            $table->integer('area_id')->index()->unsigned();
            $table->integer('empresa_id')->index()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ofertas');
    }
}
